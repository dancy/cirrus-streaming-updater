package org.wikimedia.discovery.cirrus.updater.consumer.config;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.Instant;
import java.util.List;

import org.apache.flink.api.java.utils.ParameterTool;
import org.junit.jupiter.api.Test;

import com.google.common.collect.ImmutableList;

class ConsumerConfigTest {

    final String streamConfigUrl = "https://streamconfig.local";
    final String kafkaSourceBootstrapServers = "localhost:4242";
    final String kafkaSourceGroupId = "cirrussearch.update_pipeline.consumer";

    final Instant kafkaStartTime = Instant.now();
    final Instant kafkaEndTime = Instant.now();
    final List<String> jsonSchemaUrls =
            ImmutableList.of("http://localhost:8080/json-schema1", "http://localhost:8080/json-schema2");

    final String updateStream = "update";
    private final List<String> elasticsearchUrls =
            ImmutableList.of("http://localhost:9200", "http://localhost:9300");

    @Test
    void params() {
        final ConsumerConfig config =
                ConsumerConfig.of(
                        ParameterTool.fromArgs(
                                new String[] {
                                    "--event-stream-config-url",
                                    streamConfigUrl,
                                    "--event-stream-json-schema-urls",
                                    String.join(";", jsonSchemaUrls),
                                    "--kafka-source-config.bootstrap.servers",
                                    kafkaSourceBootstrapServers,
                                    "--kafka-source-config.group.id",
                                    kafkaSourceGroupId,
                                    "--kafka-source-start-time",
                                    kafkaStartTime.toString(),
                                    "--kafka-source-end-time",
                                    kafkaEndTime.toString(),
                                    "--update-stream",
                                    updateStream,
                                    "--elasticsearch-urls",
                                    String.join(";", elasticsearchUrls)
                                }));

        assertThat(config.eventStreamConfigUrl()).isEqualTo(streamConfigUrl);
        assertThat(config.eventStreamJsonSchemaUrls()).isEqualTo(jsonSchemaUrls);
        assertThat(config.kafkaSourceBootstrapServers()).isEqualTo(kafkaSourceBootstrapServers);
        assertThat(config.kafkaSourceGroupId()).isEqualTo(kafkaSourceGroupId);
        assertThat(config.kafkaSourceStartTime()).isEqualTo(kafkaStartTime);
        assertThat(config.kafkaSourceEndTime()).isEqualTo(kafkaEndTime);

        assertThat(config.updateStream()).isEqualTo(updateStream);
        assertThat(config.elasticSearchUrls()).isEqualTo(elasticsearchUrls);
    }
}

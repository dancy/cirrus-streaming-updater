package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static com.google.common.collect.Lists.newArrayList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.connector.sink2.SinkWriter.Context;
import org.apache.flink.connector.elasticsearch.sink.RequestIndexer;
import org.apache.flink.types.Row;
import org.assertj.core.api.AssertFactory;
import org.assertj.core.api.InstanceOfAssertFactory;
import org.assertj.core.api.MapAssert;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema.Builder;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

@ExtendWith(MockitoExtension.class)
class UpdateElasticsearchEmitterTest {

    static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();
    static final EventRowTypeInfo UPDATE_TYPE_INFO = EventDataStreamUtilities.buildUpdateTypeInfo();
    static final JsonRowDeserializationSchema DESERIALIZATION_SCHEMA =
            new Builder(UPDATE_TYPE_INFO).build();

    final UpdateElasticsearchEmitter emitter = new UpdateElasticsearchEmitter(UPDATE_TYPE_INFO);

    @Mock RequestIndexer requestIndexer;

    @Mock Context context;

    @Captor ArgumentCaptor<UpdateRequest> updateRequestArgumentCaptor;
    @Captor ArgumentCaptor<DeleteRequest> deleteRequestArgumentCaptor;

    @Test
    void emit() throws IOException {
        final JsonNode events = EventDataStreamUtilities.parseJson("/update.events.json");

        emitter.emit(asRow(DESERIALIZATION_SCHEMA, events.get(0)), context, requestIndexer);
        emitter.emit(asRow(DESERIALIZATION_SCHEMA, events.get(4)), context, requestIndexer);

        Mockito.verify(requestIndexer).add(updateRequestArgumentCaptor.capture());
        Mockito.verify(requestIndexer).add(deleteRequestArgumentCaptor.capture());

        final UpdateRequest updateRequest = updateRequestArgumentCaptor.getValue();
        assertThat(updateRequest.id()).isEqualTo("147498");
        assertThat(updateRequest.index()).isEqualTo("testwiki_content");

        final DeleteRequest deleteRequest = deleteRequestArgumentCaptor.getValue();
        assertThat(deleteRequest.id()).isEqualTo("147498");
        assertThat(deleteRequest.index()).isEqualTo("testwiki_content");
    }

    @Test
    void emit_noop_set_handler_for_redirects() throws IOException {
        final JsonNode events = EventDataStreamUtilities.parseJson("/update.events.json");

        emitter.emit(asRow(DESERIALIZATION_SCHEMA, events.get(2)), context, requestIndexer);
        emitter.emit(asRow(DESERIALIZATION_SCHEMA, events.get(3)), context, requestIndexer);

        Mockito.verify(requestIndexer, Mockito.times(2)).add(updateRequestArgumentCaptor.capture());

        final List<UpdateRequest> updates = updateRequestArgumentCaptor.getAllValues();

        final UpdateRequest updateRequest = updates.get(0);
        assertThat(updateRequest.id()).isEqualTo("147498");
        assertThat(updateRequest.index()).isEqualTo("testwiki_content");
        assertThat(updateRequest.script().getLang()).isEqualTo("super_detect_noop");
        assertThat(updateRequest.script().getParams()).containsKeys("source", "handlers");
        assertThat(updateRequest.script().getParams())
                .extractingByKey(
                        "handlers", UpdateElasticsearchEmitterTest.<String, String>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo("set");
        assertThat(updateRequest.script().getParams())
                .extractingByKey(
                        "source", UpdateElasticsearchEmitterTest.<String, Object>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo(
                        ImmutableMap.of(
                                "add",
                                newArrayList(ImmutableMap.of("namespace", 2L, "title", "Redirect_Source")),
                                "max_size",
                                1024));

        final UpdateRequest deleteRequest = updates.get(1);
        assertThat(deleteRequest.id()).isEqualTo("147498");
        assertThat(deleteRequest.index()).isEqualTo("testwiki_content");
        assertThat(deleteRequest.script().getLang()).isEqualTo("super_detect_noop");
        assertThat(deleteRequest.script().getParams()).containsKeys("source", "handlers");
        assertThat(deleteRequest.script().getParams())
                .extractingByKey(
                        "handlers", UpdateElasticsearchEmitterTest.<String, String>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo("set");
        assertThat(deleteRequest.script().getParams())
                .extractingByKey(
                        "source", UpdateElasticsearchEmitterTest.<String, Object>mapAssertFactory())
                .extractingByKey("redirect")
                .isEqualTo(
                        ImmutableMap.of(
                                "remove",
                                newArrayList(ImmutableMap.of("namespace", 2L, "title", "Deleted_Redirect_Source")),
                                "max_size",
                                1024));
    }

    @Test
    void emit_fails_for_unknown_operation() {
        final Row update = UPDATE_TYPE_INFO.createEmptyRow();
        update.setField("operation", "other");

        assertThatThrownBy(() -> emitter.emit(update, context, requestIndexer))
                .isInstanceOf(UnsupportedOperationException.class);
    }

    @NotNull
    private static <KEY, VALUE>
            InstanceOfAssertFactory<Map, ? extends MapAssert<KEY, VALUE>> mapAssertFactory() {
        return new InstanceOfAssertFactory<>(
                Map.class,
                (AssertFactory<Map, MapAssert<KEY, VALUE>>) (handlers) -> new MapAssert<>(handlers));
    }

    private static Row asRow(JsonRowDeserializationSchema deserializationSchema, JsonNode createEvent)
            throws IOException {
        final byte[] bytes = OBJECT_MAPPER.writer().writeValueAsBytes(createEvent);
        return deserializationSchema.deserialize(bytes);
    }
}

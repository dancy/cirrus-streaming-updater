package org.wikimedia.discovery.cirrus.updater.consumer.graph;

import static com.google.common.collect.Maps.newHashMap;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nonnull;

import org.apache.flink.api.connector.sink2.SinkWriter.Context;
import org.apache.flink.api.java.typeutils.RowTypeInfo;
import org.apache.flink.connector.elasticsearch.sink.ElasticsearchEmitter;
import org.apache.flink.connector.elasticsearch.sink.RequestIndexer;
import org.apache.flink.types.Row;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.script.Script;
import org.elasticsearch.script.ScriptType;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields.NoopSetParameters;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowSerializationSchema;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

@SuppressFBWarnings(
        value = "IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID",
        justification = "generated serialVersionUID is considered invalid")
public class UpdateElasticsearchEmitter implements ElasticsearchEmitter<Row>, Serializable {

    private static final long serialVersionUID = -8680843379258850769L;

    private static final String LANG_NOOP = "super_detect_noop";
    private static final int REDIRECT_MAX_SIZE = 1024;

    private transient JsonRowSerializationSchema schema;

    private transient ObjectMapper mapper;

    private final RowTypeInfo updateTypeInfo;

    public UpdateElasticsearchEmitter(RowTypeInfo updateTypeInfo) {
        this.updateTypeInfo = updateTypeInfo;
    }

    @Override
    public void emit(Row element, Context context, RequestIndexer indexer) {
        try {
            switch (UpdateFields.getOperation(element)) {
                case UpdateFields.OPERATION_UPDATE_REVISION:
                    indexer.add(createUpdateRequest(element, true));
                    break;
                case UpdateFields.OPERATION_PARTIAL_UPDATE:
                    indexer.add(createUpdateRequest(element, false));
                    break;
                case UpdateFields.OPERATION_DELETE:
                    indexer.add(createDeleteRequest(element));
                    break;
                default:
                    throw new UnsupportedOperationException(
                            "Unable to emit elasticsearch command for update operation "
                                    + UpdateFields.getOperation(element));
            }
        } catch (IOException e) {
            throw new RuntimeException("Failed to encode update fields", e);
        }
    }

    @Nonnull
    private static DeleteRequest createDeleteRequest(Row element) {
        return new DeleteRequest(
                UpdateFields.getIndexName(element), Long.toString(UpdateFields.getPageId(element)));
    }

    @Nonnull
    private UpdateRequest createUpdateRequest(Row updateEvent, boolean upsert) throws IOException {
        final Row rawFields = UpdateFields.getFields(updateEvent);
        final Map<String, String> hints = UpdateFields.getNoopHints(updateEvent);
        final Map<String, Object> scriptSourceParam = toMap(rawFields);
        final Map<String, Object> upsertSource = upsert ? new HashMap<>(scriptSourceParam) : null;

        UpdateFields.getRedirectNoopSetParameters(updateEvent)
                .map(
                        parameters -> {
                            if (parameters.getMaxSize() == null) {
                                parameters.setMaxSize(REDIRECT_MAX_SIZE);
                            }
                            return parameters;
                        })
                .map(NoopSetParameters::toMap)
                .ifPresent(override -> scriptSourceParam.put(UpdateFields.REDIRECT, override));

        final Script script =
                new Script(
                        ScriptType.INLINE,
                        LANG_NOOP,
                        LANG_NOOP,
                        ImmutableMap.of("source", scriptSourceParam, "handlers", hints));

        final String index = UpdateFields.getIndexName(updateEvent);
        final String id = Long.toString(UpdateFields.getPageId(updateEvent));

        final UpdateRequest request = new UpdateRequest(index, id).script(script).retryOnConflict(5);
        if (upsertSource != null) {
            request.upsert(upsertSource);
        }
        return request;
    }

    private Map<String, Object> toMap(Row fields) throws IOException {
        if (fields == null) {
            return newHashMap();
        }
        final byte[] encodedJson = getSchema().serialize(fields);
        final ObjectMapper mapper = getMapper();
        final JsonNode decodedJson = mapper.reader().readTree(encodedJson);
        return newHashMap(
                mapper.convertValue(decodedJson, new TypeReference<Map<String, Object>>() {}));
    }

    private ObjectMapper getMapper() {
        if (mapper == null) {
            mapper = new ObjectMapper();
        }
        return mapper;
    }

    private JsonRowSerializationSchema getSchema() {
        if (schema == null) {
            schema = UpdateFields.buildFieldsRowSerializer(updateTypeInfo);
        }
        return schema;
    }
}

package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.state.ListStateDescriptor;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.streaming.api.windowing.assigners.TumblingProcessingTimeWindows;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.streaming.api.windowing.triggers.ProcessingTimeTrigger;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.streaming.runtime.operators.windowing.WindowOperator;
import org.apache.flink.streaming.runtime.operators.windowing.functions.InternalIterableProcessWindowFunction;
import org.apache.flink.streaming.runtime.streamrecord.StreamRecord;
import org.apache.flink.streaming.util.KeyedOneInputStreamOperatorTestHarness;
import org.apache.flink.streaming.util.TestHarnessUtil;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.collect.ImmutableMap;

class DeduplicateAndMergeTest {

    private static final EventRowTypeInfo EVENT_ROW_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    static final Row MOCK_RAW_FIELDS = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
    static final InputEvent.Update MOCK_UPDATE =
            InputEvent.Update.builder()
                    .noopHints(Collections.emptyMap())
                    .rawFields(MOCK_RAW_FIELDS)
                    .build();
    static final InputEvent.TargetDocument MOCK_REDIRECT_TARGET_DOCUMENT_A =
            new InputEvent.TargetDocument("domain", "wikiId", 0L, 2L);
    static final InputEvent.TargetDocument MOCK_REDIRECT_TARGET_DOCUMENT_B =
            new InputEvent.TargetDocument("domain", "wikiId", 0L, 3L);
    InputEvent.TargetDocument target = new InputEvent.TargetDocument("domain", "wikiId", 0L, 1L);
    Instant eventTime = Instant.EPOCH;

    private KeyedOneInputStreamOperatorTestHarness<PageKey, InputEvent, InputEvent> testHarness;
    private DeduplicateAndMerge<TimeWindow> dedupAndMerger;

    @Test
    void doesNothingWithOneEvent() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/bar|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactlyInAnyOrderElementsOf(inputs);
    }

    @Test
    void mergesTwoWeightedTags() throws Exception {
        List<InputEvent> results =
                process(
                        newArrayList(
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(newArrayList("foo/exists|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(newArrayList("bar/exists|1")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
    }

    @Test
    void mergesManyWeightedTags() throws Exception {
        List<InputEvent> results =
                process(
                        newArrayList(
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(newArrayList("exists|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(newArrayList("bar/a|1", "bar/b|1"))),
                                newInputEvent(
                                        ChangeType.TAGS_UPDATE,
                                        1L,
                                        InputEvent.Update.forWeightedTags(newArrayList("qqq/q|2")))));
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("exists|1", "bar/a|1", "bar/b|1", "qqq/q|2");
    }

    @Test
    void newestTagWinsWithOverlappingWeightedTagsForSameRevision() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/a|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/b|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 1, 0);
    }

    @Test
    void mergesWeightedTagsIntoUpdates() throws Exception {
        List<String> weightedTags = newArrayList("foo/exists|1");
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE, 1L, InputEvent.Update.forWeightedTags(weightedTags)));

        List<InputEvent> results = process(inputs);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
        assertThat(results.get(0).getUpdate().getRawFields()).isEqualTo(MOCK_UPDATE.getRawFields());
        assertMetrics(inputs.size(), results.size(), 1, 0);
    }

    @Test
    void mergesWeightedTagsForOlderRevisionIntoUpdates() throws Exception {
        List<String> weightedTags = newArrayList("foo/exists|1");
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE, 1L, InputEvent.Update.forWeightedTags(weightedTags)),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrderElementsOf(weightedTags);
        assertMetrics(inputs.size(), results.size(), 1, 0);
    }

    @Test
    void dropsOverlappingWeightedTagsForOlderRevision() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/a|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/b|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));

        // Same, but with reversed event time ordering
        inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                2L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/b|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/a|1"))));
        this.createTestHarness();
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
        assertMetrics(inputs.size(), results.size(), 1, 0);
    }

    @Test
    void mergesMultipleWeightedTagsIntoUpdates() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/exists|1"))),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("bar/exists|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).hasSize(1);
        assertThat(results.get(0).getChangeType()).isEqualTo(ChangeType.REV_BASED_UPDATE);
        assertThat(results.get(0).getUpdate().getWeightedTags())
                .containsExactlyInAnyOrder("foo/exists|1", "bar/exists|1");
        assertThat(results.get(0).getUpdate().getRawFields()).isEqualTo(MOCK_UPDATE.getRawFields());
        assertMetrics(inputs.size(), results.size(), 2, 0);
    }

    @Test
    void emitsHighestRevisionForMultipleRevisions() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(inputs);

        this.createTestHarness();
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));

        // the older revision having a newer event time shouldn't matter
        inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        this.createTestHarness();
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void emitsMostRecentEventForSameRevision() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        // note that event time increases on each call to newInputEvent
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));

        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        // reset test state
        this.createTestHarness();

        // ingesting in reverse of event time shouldn't matter
        Collections.reverse(inputs);
        results = process(inputs);
        assertThat(results).containsExactly(inputs.get(0));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void dedpulicatesRevBasedUpdates() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void passesThroughDeletes() throws Exception {
        List<InputEvent> inputs = newArrayList(newInputEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactlyInAnyOrderElementsOf(inputs);
        assertMetrics(inputs.size(), results.size(), 0, 0);
    }

    @Test
    void dropsTagUpdatesWhenDeleted() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("example/a|1"))),
                        newInputEvent(ChangeType.PAGE_DELETE, 2L, null),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("other/a|1"))));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 2, 0);
    }

    @Test
    void dropsOldRevUpdatesWhenDeleted() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(ChangeType.PAGE_DELETE, 2L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void dropsDeletePriorToUndelete() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void deduplicatesDeletes() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null),
                        newInputEvent(ChangeType.PAGE_DELETE, 1L, null));
        List<InputEvent> results = process(inputs);
        assertThat(results).containsExactly(inputs.get(1));
        assertMetrics(inputs.size(), results.size(), 0, 1);
    }

    @Test
    void mergesHintsWhenEqual() throws Exception {
        Map<String, String> hint = new HashMap<>();
        hint.put("field", "value");
        hint.put("weighted_tags", "multilist");

        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.REV_BASED_UPDATE,
                                1L,
                                InputEvent.Update.builder().noopHints(hint).rawFields(MOCK_RAW_FIELDS).build()),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(hint);
        assertMetrics(inputs.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesNonOverlapingHints() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(
                                ChangeType.REV_BASED_UPDATE,
                                1L,
                                InputEvent.Update.builder()
                                        .noopHints(Collections.singletonMap("field", "value"))
                                        .rawFields(MOCK_RAW_FIELDS)
                                        .build()),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("foo/a|1"))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs)
                .extracting(r -> r.getUpdate().getNoopHints())
                .containsExactlyInAnyOrder(
                        ImmutableMap.of(
                                "field", "value",
                                "weighted_tags", "multilist"));
        assertMetrics(inputs.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesRedirectAdd() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A), newHashSet())));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs).hasSize(1);
        final InputEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove()).isNull();
    }

    @Test
    void mergesRedirectRemove() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(), newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs).hasSize(1);
        final InputEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).isNull();
        assertThat(merged.getUpdate().getRedirectRemove()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(inputs.size(), outputs.size(), 1, 0);
    }

    @Test
    void mergesRedirectChanges() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A), newHashSet())),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(), newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs).hasSize(1);
        final InputEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove()).contains(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(inputs.size(), outputs.size(), 2, 0);
    }

    @Test
    void mergesRedirectChangesRespectingEventTime() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 1L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs).hasSize(1);
        final InputEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertThat(merged.getUpdate().getRedirectRemove())
                .containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertMetrics(inputs.size(), outputs.size(), 2, 0);
    }

    @Test
    void mergesRedirectChangesRespectingRevision() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                2L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B))),
                        newInputEvent(
                                ChangeType.REDIRECT_UPDATE,
                                1L,
                                InputEvent.Update.forRedirect(
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_B),
                                        newHashSet(MOCK_REDIRECT_TARGET_DOCUMENT_A))));
        List<InputEvent> outputs = process(inputs);
        assertThat(outputs).hasSize(1);
        final InputEvent merged = outputs.get(0);
        assertThat(merged.getUpdate().getNoopHints())
                .containsAllEntriesOf(ImmutableMap.of("redirect", "set"));
        assertThat(merged.getUpdate().getRedirectAdd()).containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_A);
        assertThat(merged.getUpdate().getRedirectRemove())
                .containsOnly(MOCK_REDIRECT_TARGET_DOCUMENT_B);
        assertMetrics(inputs.size(), outputs.size(), 2, 0);
    }

    @Test
    void dedupPageRerenders() throws Exception {
        List<InputEvent> inputs = newArrayList(newRerender(), newRerender());
        List<InputEvent> output = process(inputs);
        assertThat(output).hasSize(1);
        assertMetrics(inputs.size(), output.size(), 0, 1);
    }

    @Test
    void dedupRevBasedAndPageRerenders() throws Exception {
        List<InputEvent> inputs =
                newArrayList(newRerender(), newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE));
        List<InputEvent> output = process(inputs);
        assertThat(output).hasSize(1);
        InputEvent event = output.get(0);
        assertThat(event).isEqualTo(inputs.get(1));
        assertMetrics(inputs.size(), output.size(), 0, 1);
    }

    @Test
    void doNotMergeTagsUpdateAndPageRerenders() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newRerender(),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<InputEvent> output = process(inputs);
        assertThat(output).hasSize(2);
        assertThat(output.get(0)).isEqualTo(inputs.get(1));
        assertThat(output.get(1)).isEqualTo(inputs.get(0));
        assertMetrics(inputs.size(), output.size(), 0, 0);
    }

    @Test
    void dedupRevBasedTagsUpdateAndPageRerenders() throws Exception {
        List<InputEvent> inputs =
                newArrayList(
                        newRerender(),
                        newInputEvent(ChangeType.REV_BASED_UPDATE, 2L, MOCK_UPDATE),
                        newInputEvent(
                                ChangeType.TAGS_UPDATE,
                                1L,
                                InputEvent.Update.forWeightedTags(newArrayList("example/a|1"))));
        List<InputEvent> output = process(inputs);
        assertThat(output).hasSize(1);
        InputEvent revBasedMerged = output.get(0);
        assertThat(revBasedMerged.getRevId()).isEqualTo(2L);
        assertThat(revBasedMerged.getUpdate().getWeightedTags())
                .isEqualTo(inputs.get(2).getUpdate().getWeightedTags());
        assertMetrics(inputs.size(), output.size(), 1, 1);
    }

    InputEvent newInputEvent(ChangeType changeType, Long revId, InputEvent.Update update) {
        InputEvent e = new InputEvent();
        e.setChangeType(changeType);
        e.setUpdate(update);
        e.setTargetDocument(target);
        e.setRevId(revId);
        // Make event time ordering the same as creation order.
        e.setEventTime(eventTime);
        eventTime = eventTime.plus(Duration.ofMinutes(1));
        return e;
    }

    InputEvent newRerender() {
        InputEvent e = new InputEvent();
        e.setChangeType(ChangeType.PAGE_RERENDER);
        e.setTargetDocument(target);
        // Make event time ordering the same as creation order.
        e.setEventTime(eventTime);
        eventTime = eventTime.plus(Duration.ofMinutes(1));
        return e;
    }

    private void assertMetrics(int inputSize, int outputSize, int merged, int dedup) {
        assertThat(dedupAndMerger.getMerged().getCount()).isEqualTo(merged);
        assertThat(dedupAndMerger.getDeduplicated().getCount()).isEqualTo(dedup);
        assertThat(inputSize).isEqualTo(outputSize + merged + dedup);
    }

    @BeforeEach
    void createTestHarness() throws Exception {
        final int windowSize = 3;
        TypeInformation<PageKey> keyTypeInfo = TypeInformation.of(PageKey.class);
        ListStateDescriptor<InputEvent> stateDesc =
                new ListStateDescriptor<>(
                        "window-contents",
                        TypeInformation.of(InputEvent.class).createSerializer(new ExecutionConfig()));
        this.dedupAndMerger = new DeduplicateAndMerge<>();
        WindowOperator<PageKey, InputEvent, Iterable<InputEvent>, InputEvent, TimeWindow> operator =
                new WindowOperator<>(
                        TumblingProcessingTimeWindows.of(Time.seconds(windowSize)),
                        new TimeWindow.Serializer(),
                        InputEvent.KEY_SELECTOR,
                        keyTypeInfo.createSerializer(new ExecutionConfig()),
                        stateDesc,
                        new InternalIterableProcessWindowFunction<>(dedupAndMerger),
                        ProcessingTimeTrigger.create(),
                        0,
                        null);

        this.testHarness =
                new KeyedOneInputStreamOperatorTestHarness<>(
                        operator, operator.getKeySelector(), keyTypeInfo);
    }

    List<InputEvent> process(Collection<InputEvent> collection) throws Exception {
        // We don't really use the full test harness, but something has to be inplace for the metrics
        // to not blow up everything else. Could probably use a less complete mock, but this is
        // available and works well enough.
        testHarness.open();
        // Note that processing time is in ms
        testHarness.setProcessingTime(5);
        for (InputEvent event : collection) {
            testHarness.processElement(new StreamRecord<>(event));
        }
        // Necessary to close the windows
        testHarness.setProcessingTime(5000);
        return TestHarnessUtil.getRawElementsFromOutput(testHarness.getOutput());
    }
}

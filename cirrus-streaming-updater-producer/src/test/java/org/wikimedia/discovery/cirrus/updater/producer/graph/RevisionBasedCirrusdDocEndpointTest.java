package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static net.javacrumbs.jsonunit.assertj.JsonAssertions.assertThatJson;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.IOException;
import java.net.URI;

import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

class RevisionBasedCirrusdDocEndpointTest {
    private final ObjectReader reader = new ObjectMapper().reader();

    @Test
    void buildURI() {
        RevisionBasedCirrusDocEndpoint builder = new RevisionBasedCirrusDocEndpoint((i, n) -> i);
        InputEvent event = new InputEvent();
        event.setChangeType(ChangeType.REV_BASED_UPDATE);
        event.setRevId(42L);
        event.setTargetDocument(new InputEvent.TargetDocument("local.wikimedia", "junitwiki", 0L, 1L));
        final URI uri = builder.buildURI(event);
        assertThat(uri.getScheme()).isEqualTo("https");
        assertThat(uri.getHost()).isEqualTo("local.wikimedia");
        assertThat(uri.getQuery()).contains("revids=42");
    }

    @Test
    void testRevisionRequired() throws JsonProcessingException {
        RevisionBasedCirrusDocEndpoint builder = new RevisionBasedCirrusDocEndpoint((i, n) -> i);
        InputEvent event = new InputEvent();
        assertThatThrownBy(() -> builder.buildURI(event))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("getRevId() must not be null");
        JsonNode fakeNode = reader.readTree("{}");
        assertThatThrownBy(() -> builder.extractAndAugment(event, fakeNode))
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessageContaining("getRevId() must not be null");
    }

    @Test
    void testAugment() throws IOException {
        JsonNode rootNode =
                reader.readTree(
                        this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".doc.json"));
        InputEvent inputEvent = new InputEvent();
        inputEvent.setRevId(82L);
        RevisionBasedCirrusDocEndpoint builder =
                new RevisionBasedCirrusDocEndpoint(
                        (inputDoc, node) -> {
                            assertThatJson(node).node("pageid").isEqualTo("82");
                            assertThat(inputDoc).isSameAs(inputEvent);
                            return inputEvent;
                        });
        assertThat(inputEvent).isSameAs(builder.extractAndAugment(inputEvent, rootNode));
    }

    @Test
    void testRevisionMissing() throws IOException {
        JsonNode rootNode =
                reader.readTree(
                        this.getClass().getResourceAsStream(this.getClass().getSimpleName() + ".missing.json"));
        InputEvent inputEvent = new InputEvent();
        inputEvent.setRevId(82L);
        RevisionBasedCirrusDocEndpoint builder =
                new RevisionBasedCirrusDocEndpoint(
                        (inputDoc, node) -> {
                            throw new AssertionError("Should fail before");
                        });
        assertThatThrownBy(() -> builder.extractAndAugment(inputEvent, rootNode))
                .isInstanceOf(RevisionNotFoundException.class);
    }
}

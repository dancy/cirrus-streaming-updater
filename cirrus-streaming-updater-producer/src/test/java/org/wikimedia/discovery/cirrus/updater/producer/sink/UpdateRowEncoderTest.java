package org.wikimedia.discovery.cirrus.updater.producer.sink;

import static com.google.common.collect.Sets.newHashSet;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.NoSuchElementException;

import javax.annotation.Nonnull;

import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.types.Row;
import org.junit.jupiter.api.Test;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields.NoopSetParameters;
import org.wikimedia.discovery.cirrus.updater.common.serde.EventDataStreamUtilities;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.Update;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

class UpdateRowEncoderTest {
    final Instant now = Instant.now();
    private static final EventRowTypeInfo EVENT_ROW_TYPE_INFO =
            EventDataStreamUtilities.buildUpdateTypeInfo();
    private final UpdateRowEncoder encoder = new UpdateRowEncoder(EVENT_ROW_TYPE_INFO);

    private Row emptyField() {
        return EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
    }

    @Test
    void encodeAndSerialize() throws IOException {
        final Instant now = Instant.now();

        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.getTargetDocument().completeWith("omega", "testwiki_general");
        Row fields = EVENT_ROW_TYPE_INFO.createEmptySubRow(UpdateFields.FIELDS);
        fields.setField("text", "random text");
        TargetDocument redirAdded = new TargetDocument("my domain", "my wiki", 0L, 123L);
        redirAdded.setPageTitle("TargetAdded");
        TargetDocument redirDeleted = new TargetDocument("my domain", "my wiki", 0L, 124L);
        redirDeleted.setPageTitle("TargetDeleted");
        inputEvent.setUpdate(
                InputEvent.Update.builder()
                        .noopHints(Collections.singletonMap("field", "handler"))
                        .rawFields(fields)
                        .weightedTags(Arrays.asList("tag/value1", "tag/value2"))
                        .redirectAdd(Collections.singletonList(redirAdded))
                        .redirectRemove(Collections.singletonList(redirDeleted))
                        .build());
        final Row row = encoder.encodeInputEvent(inputEvent);

        assertThat(row.getField("dt")).isEqualTo(now);
        assertThat(row.getField("page_id")).isEqualTo(147498L);
        assertThat(row.getField("rev_id")).isEqualTo(551141L);
        assertThat(row.getField("domain")).isEqualTo("my.domain.local");
        assertThat(row.getField("wiki_id")).isEqualTo("mywiki_id");
        assertThat(row.getField("cirrussearch_index_name")).isEqualTo("testwiki_general");
        assertThat(row.getField("cirrussearch_cluster_group")).isEqualTo("omega");
        Row redirSetNoopField =
                row.<Row>getFieldAs(UpdateFields.NOOP_FIELDS).getFieldAs(UpdateFields.REDIRECT);
        Row[] redirAddedRows = redirSetNoopField.getFieldAs(UpdateFields.NOOP_HINTS_SET_ADD);
        Row[] redirDeletedRows = redirSetNoopField.getFieldAs(UpdateFields.NOOP_HINTS_SET_REMOVE);

        assertThat(redirAddedRows).hasSize(1);
        Row redirRow = redirAddedRows[0];
        assertThat(redirRow.<String>getFieldAs(UpdateFields.REDIRECT_TITLE))
                .isEqualTo(redirAdded.getPageTitle());
        assertThat(redirRow.<Long>getFieldAs(UpdateFields.REDIRECT_NAMESPACE))
                .isEqualTo(redirAdded.getPageNamespace());
        redirRow = redirDeletedRows[0];
        assertThat(redirRow.<String>getFieldAs(UpdateFields.REDIRECT_TITLE))
                .isEqualTo(redirDeleted.getPageTitle());
        assertThat(redirRow.<Long>getFieldAs(UpdateFields.REDIRECT_NAMESPACE))
                .isEqualTo(redirDeleted.getPageNamespace());

        final TypeSerializer<Row> rowSerializer = EVENT_ROW_TYPE_INFO.createSerializer(null);
        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);
        rowSerializer.serialize(row, target);

        assertThat(target.length()).isPositive();
    }

    @Test
    void delete() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.setChangeType(ChangeType.PAGE_DELETE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");

        assertThat(encoder.encodeInputEvent(inputEvent))
                .extracting(UpdateFields::getOperation)
                .isEqualTo(UpdateFields.OPERATION_DELETE);
    }

    @Test
    void revBasedUpdate() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(InputEvent.Update.builder().rawFields(emptyField()).build());
        assertThat(encoder.encodeInputEvent(inputEvent))
                .extracting(UpdateFields::getOperation)
                .isEqualTo(UpdateFields.OPERATION_UPDATE_REVISION);
    }

    @Test
    void redirectAdd() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        inputEvent.setChangeType(ChangeType.REDIRECT_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(Update.forRedirect(newHashSet(originalRedirect), newHashSet()));

        final NoopSetParameters<Map<String, Object>> redirects =
                UpdateFields.getRedirectNoopSetParameters(encoder.encodeInputEvent(inputEvent))
                        .orElseThrow(() -> new NoSuchElementException("Missing redirect noop parameters"));
        assertThat(redirects.getRemove()).isNull();
        assertThat(redirects.getAdd())
                .allSatisfy(
                        encodedRedirect -> {
                            assertThat(encodedRedirect)
                                    .containsEntry(
                                            UpdateFields.REDIRECT_NAMESPACE, originalRedirect.getPageNamespace());
                            assertThat(encodedRedirect)
                                    .containsEntry(UpdateFields.REDIRECT_TITLE, originalRedirect.getPageTitle());
                        });
    }

    @Test
    void redirectRemove() throws IOException {
        final InputEvent inputEvent = createInputEvent(now);
        final TargetDocument originalRedirect = new TargetDocument("domain", "wikiId", 0L, 1L);
        originalRedirect.setPageTitle("A_Redirect");
        inputEvent.setChangeType(ChangeType.REDIRECT_UPDATE);
        inputEvent.getTargetDocument().completeWith("clusterGroup", "indexName");
        inputEvent.setUpdate(Update.forRedirect(newHashSet(), newHashSet(originalRedirect)));

        final NoopSetParameters<Map<String, Object>> redirects =
                UpdateFields.getRedirectNoopSetParameters(encoder.encodeInputEvent(inputEvent))
                        .orElseThrow(() -> new NoSuchElementException("Missing redirect noop parameters"));
        assertThat(redirects.getAdd()).isNull();
        assertThat(redirects.getRemove())
                .allSatisfy(
                        encodedRedirect -> {
                            assertThat(encodedRedirect)
                                    .containsEntry(
                                            UpdateFields.REDIRECT_NAMESPACE, originalRedirect.getPageNamespace());
                            assertThat(encodedRedirect)
                                    .containsEntry(UpdateFields.REDIRECT_TITLE, originalRedirect.getPageTitle());
                        });
    }

    @Nonnull
    private static InputEvent createInputEvent(Instant now) {
        final InputEvent inputEvent = new InputEvent();
        inputEvent.setEventTime(now);
        inputEvent.setTargetDocument(
                new InputEvent.TargetDocument("my.domain.local", "mywiki_id", 0L, 147498L));
        inputEvent.setRevId(551141L);
        inputEvent.setChangeType(ChangeType.REV_BASED_UPDATE);
        return inputEvent;
    }
}

package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;

import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.test.junit5.MiniClusterExtension;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.wikimedia.discovery.cirrus.updater.common.config.ParameterToolMerger;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

@ExtendWith(MiniClusterExtension.class)
class ProducerGraphFactoryTest {

    @Test
    void createStreamGraph() throws IOException {
        ProducerConfig.builder().build();
        final ProducerConfig config =
                ProducerConfig.of(
                        ParameterToolMerger.fromDefaultsWithOverrides(
                                new String[] {
                                    ProducerGraphFactoryTest.class
                                            .getResource(
                                                    ProducerGraphFactoryTest.class.getSimpleName() + "/overrides.properties")
                                            .toString(),
                                    "--event-stream-config-url",
                                    ProducerGraphFactoryTest.class
                                            .getResource("/event-stream-config.json")
                                            .toString(),
                                    "--event-stream-json-schema-urls",
                                    ProducerGraphFactoryTest.class.getResource("/schema_repo").toString()
                                }));
        final StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        Assertions.assertThatNoException()
                .isThrownBy(() -> new ProducerGraphFactory(config).createStreamGraph(env));
    }

    @Test
    void filterInputEvents() {
        final InputEvent inputEvent = new InputEvent();

        inputEvent.setTargetDocument(new InputEvent.TargetDocument(null, null, null, null));
        assertThat(ProducerGraphFactory.filterInputEvents(inputEvent)).isFalse();

        inputEvent.setTargetDocument(new InputEvent.TargetDocument("canary", null, null, null));
        assertThat(ProducerGraphFactory.filterInputEvents(inputEvent)).isFalse();

        inputEvent.setTargetDocument(new InputEvent.TargetDocument("other", null, null, null));
        assertThat(ProducerGraphFactory.filterInputEvents(inputEvent)).isTrue();
    }
}

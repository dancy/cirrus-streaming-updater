package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.stream.Stream;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusDocFetcher;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

@ExtendWith(MockitoExtension.class)
class BypassingCirrusDocFetcherTest {

    @Mock CirrusDocFetcher<InputEvent, InputEvent> delegate;

    @Mock InputEvent inputEvent;

    @InjectMocks BypassingCirrusDocFetcher bypassingRevisionFetcher;

    static Stream<Arguments> provideCases() {
        return Stream.of(
                Arguments.of(ChangeType.TAGS_UPDATE, false),
                Arguments.of(ChangeType.REDIRECT_UPDATE, false),
                Arguments.of(ChangeType.TAGS_UPDATE, false),
                Arguments.of(ChangeType.PAGE_DELETE, false),
                Arguments.of(ChangeType.PAGE_RERENDER, false),
                Arguments.of(ChangeType.REV_BASED_UPDATE, true));
    }

    @ParameterizedTest
    @MethodSource("provideCases")
    public void test(ChangeType type, boolean fetch) {
        when(inputEvent.getChangeType()).thenReturn(type);
        bypassingRevisionFetcher.apply(inputEvent);
        verify(delegate, fetch ? times(1) : never()).apply(inputEvent);
    }

    @Test
    void close() throws IOException {
        bypassingRevisionFetcher.close();
        verify(delegate).close();
    }
}

package org.wikimedia.discovery.cirrus.updater.producer.sink;

import java.io.Serializable;
import java.util.Collection;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Supplier;

import javax.annotation.Nonnull;

import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.TargetDocument;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent.Update;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

public class UpdateRowEncoder implements Serializable {

    private final EventRowTypeInfo outputTypeInfo;

    public UpdateRowEncoder(EventRowTypeInfo outputTypeInfo) {
        this.outputTypeInfo = outputTypeInfo;
    }

    @Nonnull
    public Row encodeInputEvent(InputEvent inputEvent) {
        final Row row = outputTypeInfo.createEmptyRow();
        final InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        if (!target.isComplete()) {
            throw new IllegalArgumentException("TargetDocument is incomplete");
        }

        row.setField(UpdateFields.OPERATION, asOperation(inputEvent));

        // TODO: pass through the mediawiki request_id for debugging outputs
        row.setField("dt", inputEvent.getEventTime());
        row.setField("domain", target.getDomain());
        row.setField("wiki_id", target.getWikiId());

        row.setField(UpdateFields.INDEX_NAME, target.getIndexName());
        row.setField(UpdateFields.PAGE_ID, target.getPageId());
        row.setField(UpdateFields.NAMESPACE_ID, target.getPageNamespace());
        row.setField(UpdateFields.REV_ID, inputEvent.getRevId());
        row.setField(UpdateFields.CLUSTER_GROUP, target.getClusterGroup());

        InputEvent.Update update = inputEvent.getUpdate();
        if (update == null) {
            if (inputEvent.getChangeType() == ChangeType.PAGE_DELETE) {
                return row;
            }
            throw new IllegalArgumentException("No update provided to non-delete operation");
        }

        if (update.getNoopHints() != null) {
            row.setField(UpdateFields.NOOP_HINTS, update.getNoopHints());
        }
        Row fields = update.getRawFields();
        if (fields != null && fields.getFieldAs(UpdateFields.WEIGHTED_TAGS) != null) {
            throw new IllegalStateException(
                    "Cannot create output Row for "
                            + inputEvent.getTargetDocument()
                            + " weighted_tags field must be null.");
        }
        if (update.getWeightedTags() != null) {
            if (fields == null) {
                fields = outputTypeInfo.createEmptySubRow(UpdateFields.FIELDS);
            }
            // we don't attempt to merge with an existing value as this should never
            // be provided by cirrusdoc.
            fields.setField(UpdateFields.WEIGHTED_TAGS, update.getWeightedTags().toArray(new String[0]));
        }

        final NoopFieldsRowSupplier noopFieldsRowSupplier =
                new NoopFieldsRowSupplier(outputTypeInfo::createEmptySubRow);

        encodeRedirectUpdate(update, noopFieldsRowSupplier);

        noopFieldsRowSupplier
                .getRow()
                .ifPresent(noopFields -> row.setField(UpdateFields.NOOP_FIELDS, noopFields));

        return row;
    }

    private void encodeRedirectUpdate(Update update, Supplier<Row> noopFieldsRowSupplier) {
        final String noopFieldsRedirectPath =
                fieldPath(UpdateFields.NOOP_FIELDS, UpdateFields.REDIRECT);
        final Row noopFieldsRedirect = outputTypeInfo.createEmptySubRow(noopFieldsRedirectPath);
        Optional.ofNullable(update.getRedirectAdd())
                .filter(add -> !add.isEmpty())
                .map(
                        add ->
                                encodeRedirects(
                                        add, fieldPath(noopFieldsRedirectPath, UpdateFields.NOOP_HINTS_SET_ADD)))
                .ifPresent(
                        add -> {
                            noopFieldsRedirect.setField(UpdateFields.NOOP_HINTS_SET_ADD, add);
                            noopFieldsRowSupplier.get().setField(UpdateFields.REDIRECT, noopFieldsRedirect);
                        });
        Optional.ofNullable(update.getRedirectRemove())
                .filter(remove -> !remove.isEmpty())
                .map(
                        remove ->
                                encodeRedirects(
                                        remove, fieldPath(noopFieldsRedirectPath, UpdateFields.NOOP_HINTS_SET_REMOVE)))
                .ifPresent(
                        remove -> {
                            noopFieldsRedirect.setField(UpdateFields.NOOP_HINTS_SET_REMOVE, remove);
                            noopFieldsRowSupplier.get().setField(UpdateFields.REDIRECT, noopFieldsRedirect);
                        });
    }

    private Row[] encodeRedirects(Collection<TargetDocument> redirects, String addSubRowPath) {
        return redirects.stream()
                .map(redirectTarget -> encodeRedirect(redirectTarget, addSubRowPath))
                .distinct()
                .toArray(Row[]::new);
    }

    private Row encodeRedirect(TargetDocument redirectTarget, String subRowPath) {
        final Row redirect = outputTypeInfo.createEmptySubRow(subRowPath);
        redirect.setField(UpdateFields.REDIRECT_NAMESPACE, redirectTarget.getPageNamespace());
        redirect.setField(UpdateFields.REDIRECT_TITLE, redirectTarget.getPageTitle());
        return redirect;
    }

    private static String fieldPath(String... fields) {
        return String.join(".", fields);
    }

    private static String asOperation(InputEvent inputEvent) {
        switch (inputEvent.getChangeType()) {
            case PAGE_DELETE:
                return UpdateFields.OPERATION_DELETE;
            case REV_BASED_UPDATE:
                return UpdateFields.OPERATION_UPDATE_REVISION;
            case TAGS_UPDATE:
            case REDIRECT_UPDATE:
                return UpdateFields.OPERATION_PARTIAL_UPDATE;
            default:
                final InputEvent.TargetDocument target = inputEvent.getTargetDocument();
                throw new IllegalArgumentException(
                        "No operation mapping for page-change-type "
                                + inputEvent.getChangeType()
                                + " of page-id "
                                + target.getPageId()
                                + " in "
                                + target.getWikiId());
        }
    }

    /**
     * Supplies a {@code noop_fields} row on demand.
     *
     * <p>Allows for sparse updates.
     */
    private static class NoopFieldsRowSupplier implements Supplier<Row> {

        private final Function<String, Row> rowFactory;

        private Row row;

        NoopFieldsRowSupplier(Function<String, Row> rowFactory) {
            this.rowFactory = rowFactory;
        }

        @Override
        public Row get() {
            if (row == null) {
                row = rowFactory.apply(UpdateFields.NOOP_FIELDS);
            }
            return row;
        }

        public Optional<Row> getRow() {
            return Optional.ofNullable(row);
        }
    }
}

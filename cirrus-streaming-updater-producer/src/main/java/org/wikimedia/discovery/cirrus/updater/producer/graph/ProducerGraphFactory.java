package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static java.util.Collections.singleton;
import static org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig.UPDATER_OUTPUT_SCHEMA_VERSION;
import static org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig.UPDATER_OUTPUT_STREAM_NAME;

import java.time.Duration;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.StreamSupport;

import org.apache.flink.api.common.eventtime.WatermarkStrategy;
import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.connector.sink2.Sink;
import org.apache.flink.connector.kafka.sink.KafkaSinkBuilder;
import org.apache.flink.connector.kafka.source.KafkaSource;
import org.apache.flink.connector.kafka.source.KafkaSourceBuilder;
import org.apache.flink.connector.kafka.source.enumerator.initializer.OffsetsInitializer;
import org.apache.flink.streaming.api.datastream.AsyncDataStream.OutputMode;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.datastream.DataStreamUtils;
import org.apache.flink.streaming.api.datastream.KeyedStream;
import org.apache.flink.streaming.api.datastream.SingleOutputStreamOperator;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.streaming.api.operators.async.AsyncWaitOperatorFactory;
import org.apache.flink.streaming.api.windowing.assigners.TumblingEventTimeWindows;
import org.apache.flink.streaming.api.windowing.assigners.WindowStagger;
import org.apache.flink.streaming.api.windowing.time.Time;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.apache.flink.util.Preconditions;
import org.apache.flink.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.common.TransformOperator;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusDocFetcher;
import org.wikimedia.discovery.cirrus.updater.common.graph.FetchResult;
import org.wikimedia.discovery.cirrus.updater.common.graph.FetchResultTypeInformation;
import org.wikimedia.discovery.cirrus.updater.common.graph.RetryingAsyncFunction;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.discovery.cirrus.updater.producer.config.InputEventSource;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEventTypeInfo;
import org.wikimedia.discovery.cirrus.updater.producer.sink.UpdateRowEncoder;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;
import org.wikimedia.eventutilities.flink.formats.json.KafkaRecordTimestampStrategy;
import org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Lists;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.SneakyThrows;

@SuppressWarnings({"checkstyle:classfanoutcomplexity"})
public class ProducerGraphFactory {

    public static final TypeInformation<PageKey> KEY_TYPE_INFO = TypeInformation.of(PageKey.class);
    private final ProducerConfig config;

    private final EventDataStreamFactory eventDataStreamFactory;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private transient TypeInformation<InputEvent> inputEventTypeInfo;

    public ProducerGraphFactory(ProducerConfig config) {
        this.config = config;
        this.eventDataStreamFactory =
                EventDataStreamFactory.from(
                        config.eventStreamJsonSchemaUrls(), config.eventStreamConfigUrl());
    }

    public static boolean filterInputEvents(InputEvent inputEvent) {
        InputEvent.TargetDocument target = inputEvent.getTargetDocument();
        return target.getDomain() != null && !target.getDomain().equals("canary");
    }

    public List<InputEventSource> sources() {
        // Could be more dynamically sourced, but for now hard code the set of streams we will
        // listen to and their configurations.
        return Lists.newArrayList(
                InputEventSource.builder()
                        .streamName(config.pageChangeStream())
                        .converter(InputEventConverters::fromPageChange)
                        .augmentWithCirrusDoc(true)
                        .isRevisionBased(true)
                        .build(),
                InputEventSource.builder()
                        .streamName(config.articleTopicStream())
                        .converter(
                                row ->
                                        singleton(
                                                InputEventConverters.fromRevisionScoring(
                                                        row, "classification.ores.articletopic")))
                        .isRevisionBased(true)
                        .build(),
                InputEventSource.builder()
                        .streamName(config.draftTopicStream())
                        .converter(
                                row ->
                                        singleton(
                                                InputEventConverters.fromRevisionScoring(
                                                        row, "classification.ores.drafttopic")))
                        .isRevisionBased(true)
                        .build(),
                InputEventSource.builder()
                        .streamName(config.recommendationCreateStream())
                        .converter(row -> singleton(InputEventConverters.fromRecommendationCreate(row)))
                        .build());
    }

    public DataStream<InputEvent> createDataStreamSource(
            StreamExecutionEnvironment env,
            String streamName,
            SerializableFunction<Row, Iterable<InputEvent>> converter,
            FilterFunction<InputEvent> filter) {
        KafkaSourceBuilder<Row> sourceBuilder =
                eventDataStreamFactory.kafkaSourceBuilder(
                        streamName, config.kafkaSourceBootstrapServers(), config.kafkaSourceGroupId());
        setKafkaOffsetBounds(sourceBuilder);
        sourceBuilder.setProperties(config.kafkaSourceProperties());
        KafkaSource<Row> source = sourceBuilder.build();

        WatermarkStrategy<Row> wmStrategy =
                WatermarkStrategy.<Row>forBoundedOutOfOrderness(Duration.ofSeconds(30))
                        // set idleness of the source, usefull to not block the stream if some sources might
                        // become idle
                        .withIdleness(Duration.ofSeconds(30));

        return env.fromSource(source, wmStrategy, streamName + "-source")
                .flatMap(new ConvertFilterFlatMap(converter, filter), createInputEventTypeInfo())
                .map(new IngestionTimeAssigner(config.clock()), createInputEventTypeInfo())
                .filter(ProducerGraphFactory::filterInputEvents);
    }

    public Sink<Row> createDataSink() {
        KafkaSinkBuilder<Row> sinkBuilder =
                eventDataStreamFactory.kafkaSinkBuilder(
                        UPDATER_OUTPUT_STREAM_NAME,
                        UPDATER_OUTPUT_SCHEMA_VERSION,
                        config.kafkaSinkBootstrapServers(),
                        config.fetchSuccessTopic(),
                        KafkaRecordTimestampStrategy.FLINK_RECORD_EVENT_TIME);
        sinkBuilder.setKafkaProducerConfig(config.kafkaSinkProperties());
        return sinkBuilder.build();
    }

    public EventRowTypeInfo createOutputTypeInfo() {
        return eventDataStreamFactory.rowTypeInfo(
                UPDATER_OUTPUT_STREAM_NAME, UPDATER_OUTPUT_SCHEMA_VERSION);
    }

    private TypeInformation<InputEvent> createInputEventTypeInfo() {
        if (inputEventTypeInfo == null) {
            inputEventTypeInfo = InputEventTypeInfo.create(createOutputTypeInfo());
        }
        return inputEventTypeInfo;
    }

    public TransformOperator<InputEvent, FetchResult<InputEvent, InputEvent>>
            createFetchRevisionOperator() {
        final CirrusDocFetcher<InputEvent, InputEvent> revisionFetcher =
                new CirrusDocFetcher<>(
                        config.fetchRequestTimeout(),
                        new RevisionBasedCirrusDocEndpoint(
                                InputEventConverters.cirrusDocAugmenter(
                                        objectMapper, createOutputTypeInfo().getTypeAt(UpdateFields.FIELDS))),
                        config.httpRoutes());

        final BypassingCirrusDocFetcher bypassingRevisionFetcher =
                new BypassingCirrusDocFetcher(revisionFetcher);

        final LagAwareRetryPredicate revisionFetcherRetryPredicate =
                new LagAwareRetryPredicate(
                        config.retryFetchMaxAge(), config.clock(), config.retryFetchMax());

        final RetryingAsyncFunction<
                        InputEvent, InputEvent, BypassingCirrusDocFetcher, LagAwareRetryPredicate>
                retryingFetchRevisionFunction =
                        new RetryingAsyncFunction<>(bypassingRevisionFetcher, revisionFetcherRetryPredicate);

        final long retryFetchFailTimeout =
                (long)
                        (Math.max(
                                        (config.retryFetchMaxAge().toMillis()
                                                + config.fetchRequestTimeout().toMillis()),
                                        IntStream.range(0, config.retryFetchMax())
                                                .mapToObj(retryingFetchRevisionFunction::calculateDelay)
                                                .mapToLong(Duration::toMillis)
                                                .map(delay -> delay + config.fetchRequestTimeout().toMillis())
                                                .sum())
                                * 1.2);
        // prefer ORDERED so that we slow-down everyone even events that do bypass the fetch operator
        final AsyncWaitOperatorFactory<InputEvent, FetchResult<InputEvent, InputEvent>>
                fetchRevisionOperatorFactory =
                        new AsyncWaitOperatorFactory<>(
                                retryingFetchRevisionFunction,
                                retryFetchFailTimeout,
                                config.retryFetchQueueCapacity(),
                                OutputMode.ORDERED);

        TypeInformation<InputEvent> inputEventType = createInputEventTypeInfo();
        final TypeInformation<FetchResult<InputEvent, InputEvent>> enrichedTypeInformation =
                FetchResultTypeInformation.create(inputEventType, inputEventType);

        return new TransformOperator<>(
                "enrich-page-change-with-revision", enrichedTypeInformation, fetchRevisionOperatorFactory);
    }

    public Sink<Row> createFetchFailureSink() {
        KafkaSinkBuilder<Row> sinkBuilder =
                eventDataStreamFactory.kafkaSinkBuilder(
                        ProducerConfig.FETCH_FAILURE_STREAM,
                        ProducerConfig.FETCH_FAILURE_SCHEMA_VERSION,
                        config.kafkaSinkBootstrapServers(),
                        config.fetchFailureTopic(),
                        KafkaRecordTimestampStrategy.FLINK_RECORD_EVENT_TIME);

        return sinkBuilder.build();
    }

    public MapFunction<InputEvent, InputEvent> createNamespaceIndexMapper() {
        return new CirrusNamespaceIndexMap(config.httpRoutes(), config.fetchRequestTimeout());
    }

    public ProcessFunction<FetchResult<InputEvent, InputEvent>, InputEvent> createFetchFailureRouter(
            OutputTag<Row> tag) {
        return new RouteFetchFailures(tag, config.pipeline());
    }

    public OutputTag<Row> createFetchFailureOutputTag() {
        return new OutputTag<>(
                "fetch_failures",
                eventDataStreamFactory.rowTypeInfo(
                        ProducerConfig.FETCH_FAILURE_STREAM, ProducerConfig.FETCH_FAILURE_SCHEMA_VERSION));
    }

    public OutputTag<InputEvent> createLateEventTag() {
        return new OutputTag<>("late_events", createInputEventTypeInfo());
    }

    @SuppressWarnings("unchecked")
    private <T> DataStream<T> union(DataStream<T> head, Collection<DataStream<T>> tail) {
        return head.union(tail.toArray(new DataStream[0]));
    }

    private DataStream<InputEvent> union(List<DataStream<InputEvent>> sources) {
        Preconditions.checkArgument(!sources.isEmpty(), "sources must not be empty");
        return union(sources.get(0), sources.subList(1, sources.size()));
    }

    /**
     * Key the stream via PageKey, attach the fetch operation and route fetch failures to a
     * side-output.
     */
    private KeyedStream<InputEvent, PageKey> fetch(DataStream<InputEvent> stream) {
        final OutputTag<Row> fetchFailureOutputTag = createFetchFailureOutputTag();
        final Sink<Row> fetchFailureSink = createFetchFailureSink();
        final ProcessFunction<FetchResult<InputEvent, InputEvent>, InputEvent> fetchFailureRouter =
                createFetchFailureRouter(fetchFailureOutputTag);
        final TransformOperator<InputEvent, FetchResult<InputEvent, InputEvent>>
                fetchRevisionTransformParameters = createFetchRevisionOperator();
        SingleOutputStreamOperator<InputEvent> augmented =
                stream
                        .keyBy(InputEvent.KEY_SELECTOR, KEY_TYPE_INFO)
                        .transform(
                                fetchRevisionTransformParameters.operatorName,
                                fetchRevisionTransformParameters.outTypeInformation,
                                fetchRevisionTransformParameters.operatorFactory)
                        .process(fetchFailureRouter, createInputEventTypeInfo());
        augmented.getSideOutput(fetchFailureOutputTag).sinkTo(fetchFailureSink);
        return DataStreamUtils.reinterpretAsKeyedStream(
                augmented, InputEvent.KEY_SELECTOR, KEY_TYPE_INFO);
    }

    /** Attempt to reorder the stream based on rev_id and event time. */
    private DataStream<InputEvent> reorderAndOptimize(KeyedStream<InputEvent, PageKey> stream) {
        OutputTag<InputEvent> lateEventTag = createLateEventTag();
        SingleOutputStreamOperator<InputEvent> combined =
                stream
                        .window(
                                TumblingEventTimeWindows.of(
                                        config.windowSize(), Time.minutes(0), WindowStagger.NATURAL))
                        // afaict we do not want to set an allowed lateness, that would potentially
                        // double-process the merge windows. Instead, send late events to a side
                        // output that will be processed without merging.
                        .sideOutputLateData(lateEventTag)
                        // the deduplicate part could have happened earlier, prior to fetching cirrusdoc,
                        // but we delay it for two reasons: any window prior to fetch would prevent the fetch
                        // from applying backpressure on ingestion, and because we aren't sure how to align
                        // multiple windows with WindowStagger.NATURAL.
                        .process(new DeduplicateAndMerge<>(), createInputEventTypeInfo());

        // Is this really safe? These events will be processed out of order which could
        // run a delete and undelete in the wrong order. Should be rare, but possible.
        return combined.union(combined.getSideOutput(lateEventTag));
    }

    private DataStream<InputEvent> namespaceAndIndexMapper(DataStream<InputEvent> stream) {
        final MapFunction<InputEvent, InputEvent> namespaceIndexMapper = createNamespaceIndexMapper();
        return stream.map(namespaceIndexMapper, createInputEventTypeInfo());
    }

    private void sink(DataStream<InputEvent> stream) {
        final EventRowTypeInfo outputTypeInfo = createOutputTypeInfo();
        final UpdateRowEncoder updateRowEncoder = new UpdateRowEncoder(outputTypeInfo);
        Sink<Row> updateSink = createDataSink();
        stream.map(updateRowEncoder::encodeInputEvent, outputTypeInfo).sinkTo(updateSink);
    }

    public void createStreamGraph(StreamExecutionEnvironment env) {
        List<DataStream<InputEvent>> sources =
                sources().stream()
                        .map(
                                s ->
                                        createDataStreamSource(env, s.getStreamName(), s.getConverter(), s.getFilter()))
                        .collect(Collectors.toList());
        if (sources.isEmpty()) {
            throw new IllegalArgumentException("No sources available");
        }

        // We union all sources and send them to the fetch operation, reason is that we don't want to
        // advance streams that do not require fetching too quickly, this might skew the event times.
        // This will obviously impact throughput but the other strategy would have pushed many events to
        // the next steps that requires buffering these in a window. In short, we prefer to have a
        // non-optimal throughput vs many windows waiting for the slow events to arrive.
        KeyedStream<InputEvent, PageKey> enriched = fetch(union(sources));
        DataStream<InputEvent> optimized = reorderAndOptimize(enriched);
        DataStream<InputEvent> withNamespaceAndIndex = namespaceAndIndexMapper(optimized);
        sink(withNamespaceAndIndex);
    }

    private void setKafkaOffsetBounds(KafkaSourceBuilder<Row> sourceBuilder) {
        if (config.kafkaSourceStartTime() != null) {
            sourceBuilder.setStartingOffsets(
                    OffsetsInitializer.timestamp(config.kafkaSourceStartTime().toEpochMilli()));
        }
        if (config.kafkaSourceEndTime() != null) {
            sourceBuilder.setBounded(
                    OffsetsInitializer.timestamp(config.kafkaSourceEndTime().toEpochMilli()));
        }
    }

    /** Flat-maps a source event ({@link Row}) into {@code 0+} {@link InputEvent InputEvents}. */
    @SuppressFBWarnings(
            value = "IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID",
            justification = "generated serialVersionUID is considered invalid")
    private static final class ConvertFilterFlatMap implements FlatMapFunction<Row, InputEvent> {

        private static final long serialVersionUID = 6615660944130209703L;
        private final SerializableFunction<Row, Iterable<InputEvent>> converter;
        private final FilterFunction<InputEvent> filter;

        private ConvertFilterFlatMap(
                SerializableFunction<Row, Iterable<InputEvent>> converter,
                FilterFunction<InputEvent> filter) {
            this.converter = converter;
            this.filter = filter;
        }

        @Override
        public void flatMap(Row row, Collector<InputEvent> collector) {
            StreamSupport.stream(converter.apply(row).spliterator(), true)
                    .filter(Objects::nonNull)
                    .filter(this::filter)
                    .forEach(collector::collect);
        }

        @SneakyThrows
        private boolean filter(InputEvent inputEvent) {
            return filter == null || filter.filter(inputEvent);
        }
    }
}

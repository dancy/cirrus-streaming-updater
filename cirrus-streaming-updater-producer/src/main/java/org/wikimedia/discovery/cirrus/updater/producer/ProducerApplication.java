/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.wikimedia.discovery.cirrus.updater.producer;

import org.apache.flink.api.java.utils.ParameterTool;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.slf4j.LoggerFactory;
import org.wikimedia.discovery.cirrus.updater.common.config.ParameterToolMerger;
import org.wikimedia.discovery.cirrus.updater.producer.config.ProducerConfig;
import org.wikimedia.discovery.cirrus.updater.producer.graph.ProducerGraphFactory;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("checkstyle:HideUtilityClassConstructor")
public class ProducerApplication {

    public static final String JOB_NAME = "cirrus-streaming-updater-producer";

    public static void main(String[] args) throws Exception {
        final ParameterTool params = ParameterToolMerger.fromDefaultsWithOverrides(args);
        StreamExecutionEnvironment env = StreamExecutionEnvironment.getExecutionEnvironment();
        env.getConfig().setGlobalJobParameters(params);
        // Generic types do use Kryo as a fallback, since kryo has multiple drawbacks:
        // - perf is generally worse:
        // https://flink.apache.org/2020/04/15/flink-serialization-tuning-vol.-1-choosing-your-serializer-if-you-can/
        // - schema evolutions are not supported:
        // https://nightlies.apache.org/flink/flink-docs-release-1.17/docs/dev/datastream/fault-tolerance/serialization/schema_evolution/
        // we prefer to avoid it.
        // Since it's easy to forget to declare the proper TypeInformation when chaining operators we
        // tell flink to explicitly fail whenever it detects that Kryo is going to be used.
        env.getConfig().disableGenericTypes();

        final ProducerConfig config = ProducerConfig.of(params);
        new ProducerGraphFactory(config).createStreamGraph(env);

        if (config.dryRun()) {
            LoggerFactory.getLogger(ProducerApplication.class).info("DRY RUN DONE");
            return;
        }
        env.execute(JOB_NAME);
    }
}

package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import org.wikimedia.discovery.cirrus.updater.common.async.CompletableFutureBackPorts;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusDocFetcher;
import org.wikimedia.discovery.cirrus.updater.producer.model.ChangeType;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

/**
 * Wrapper around {@link CirrusDocFetcher} that bypasses fetching revisions under certain
 * conditions.
 *
 * <p>Allows skipping {@link ChangeType#PAGE_DELETE}s while maintaining the order of events from
 * flink's perspective.
 */
public class BypassingCirrusDocFetcher
        implements Function<InputEvent, CompletionStage<InputEvent>>, Serializable, Closeable {

    private final CirrusDocFetcher<InputEvent, InputEvent> delegate;

    public BypassingCirrusDocFetcher(CirrusDocFetcher<InputEvent, InputEvent> delegate) {
        this.delegate = delegate;
    }

    @Override
    public CompletionStage<InputEvent> apply(InputEvent inputEvent) {
        return inputEvent.getChangeType() == ChangeType.REV_BASED_UPDATE
                ? delegate.apply(inputEvent)
                : CompletableFutureBackPorts.completedFuture(inputEvent);
    }

    @Override
    public void close() throws IOException {
        delegate.close();
    }
}

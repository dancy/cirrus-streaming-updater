package org.wikimedia.discovery.cirrus.updater.producer.model;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.model.PageKey;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
public class InputEvent {
    public static final KeySelector<InputEvent, PageKey> KEY_SELECTOR =
            i -> new PageKey(i.getTargetDocument().getWikiId(), i.getTargetDocument().getPageId());

    ChangeType changeType;
    String requestId;
    String eventStream;
    Instant eventTime;
    Long revId;
    Instant ingestionTime;
    TargetDocument targetDocument;
    Update update;

    /** Targeting information for a single document in elasticsearch. */
    @Data
    public static class TargetDocument {
        private String domain;
        private String wikiId;
        private Long pageNamespace;
        private Long pageId;
        private String clusterGroup;
        private String indexName;
        private String pageTitle;

        public TargetDocument() {}

        public TargetDocument(String domain, String wikiId, Long pageNamespace, Long pageId) {
            this.domain = domain;
            this.wikiId = wikiId;
            this.pageNamespace = pageNamespace;
            this.pageId = pageId;
        }

        public boolean isComplete() {
            return clusterGroup != null && indexName != null;
        }

        public void completeWith(String clusterGroup, String indexName) {
            this.clusterGroup = clusterGroup;
            this.indexName = indexName;
        }
    }

    @Data
    @AllArgsConstructor
    @Builder(toBuilder = true)
    public static class Update {

        public static final Map<String, String> WEIGHTED_TAGS_HINT =
                Collections.singletonMap(UpdateFields.WEIGHTED_TAGS, "multilist");

        public static final Map<String, String> REDIRECT_HINT =
                Collections.singletonMap(UpdateFields.REDIRECT, "set");

        private Map<String, String> noopHints;

        private Row rawFields;
        private List<String> weightedTags;

        private List<TargetDocument> redirectRemove;
        private List<TargetDocument> redirectAdd;

        public Update() {}

        public static Update forWeightedTags(Collection<String> weightedTags) {
            return builder()
                    .noopHints(WEIGHTED_TAGS_HINT)
                    .weightedTags(
                            weightedTags == null || weightedTags.isEmpty() ? null : asList(weightedTags))
                    .build();
        }

        public static Update forRedirect(
                Collection<TargetDocument> add, Collection<TargetDocument> remove) {
            return builder()
                    .noopHints(REDIRECT_HINT)
                    .redirectAdd(add == null || add.isEmpty() ? null : asList(add))
                    .redirectRemove(remove == null || remove.isEmpty() ? null : asList(remove))
                    .build();
        }

        private static <T> List<T> asList(Collection<T> weightedTags) {
            return weightedTags instanceof List ? (List<T>) weightedTags : new ArrayList<>(weightedTags);
        }
    }
}

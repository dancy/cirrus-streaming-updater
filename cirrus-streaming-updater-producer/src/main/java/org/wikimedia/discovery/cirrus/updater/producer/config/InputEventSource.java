package org.wikimedia.discovery.cirrus.updater.producer.config;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.function.SerializableFunction;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;
import lombok.experimental.Accessors;

@AllArgsConstructor
@Builder
@Data
public class InputEventSource {

    @NonNull private final String streamName;
    @NonNull private final SerializableFunction<Row, Iterable<InputEvent>> converter;

    private final FilterFunction<InputEvent> filter;

    @Accessors(fluent = true)
    @Builder.Default
    private final boolean isRevisionBased = false;

    @Accessors(fluent = true)
    @Builder.Default
    private final boolean augmentWithCirrusDoc = false;
}

package org.wikimedia.discovery.cirrus.updater.producer.graph;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.collect.ImmutableList.copyOf;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.util.Arrays;
import java.util.List;

import org.apache.flink.streaming.api.functions.ProcessFunction;
import org.apache.flink.types.Row;
import org.apache.flink.util.Collector;
import org.apache.flink.util.OutputTag;
import org.wikimedia.discovery.cirrus.updater.common.graph.FetchResult;
import org.wikimedia.discovery.cirrus.updater.common.graph.InvalidMWApiResponseException;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

public class RouteFetchFailures
        extends ProcessFunction<FetchResult<InputEvent, InputEvent>, InputEvent> {
    private final OutputTag<Row> errorOutputTag;
    private final String pipeline;
    private final EventRowTypeInfo rowTypeInfo;

    public RouteFetchFailures(OutputTag<Row> errorOutputTag, String pipeline) {
        this.errorOutputTag = errorOutputTag;
        checkArgument(
                errorOutputTag.getTypeInfo() instanceof EventRowTypeInfo,
                "errorOutputTag must have a TypeInfo of type EventRowTypeInfo");
        rowTypeInfo = (EventRowTypeInfo) errorOutputTag.getTypeInfo();
        this.pipeline = pipeline;
    }

    @Override
    public void processElement(
            FetchResult<InputEvent, InputEvent> o, Context context, Collector<InputEvent> collector) {
        // We expect an Either monad here between output and failure.
        checkArgument(
                o.getOutput() != null || o.hasError(),
                "RetryContext should either have an output or a failure.");
        if (o.hasError()) {
            InputEvent event = o.getInput();
            InputEvent.TargetDocument target = event.getTargetDocument();
            Row failure = rowTypeInfo.createEmptyRow();
            failure.setField("pipeline", pipeline);
            failure.setField("wiki_id", target.getWikiId());
            failure.setField("page_id", target.getPageId());
            failure.setField("page_title", target.getPageTitle());
            failure.setField("rev_id", event.getRevId());
            failure.setField("namespace_id", target.getPageNamespace());
            failure.setField("original_event_stream", event.getEventStream());
            failure.setField("original_event_time", event.getEventTime());
            failure.setField("original_ingestion_time", event.getIngestionTime());
            Row meta = rowTypeInfo.createEmptySubRow("meta");
            meta.setField("domain", target.getDomain());
            meta.setField("request_id", event.getRequestId());
            failure.setField("meta", meta);

            ErrorType errorType =
                    Arrays.stream(ErrorType.values())
                            .filter(er -> er.matches(o.getErrorType()))
                            .findFirst()
                            .orElseThrow(
                                    () ->
                                            new IllegalArgumentException(
                                                    "Unsupported exception type: "
                                                            + o.getErrorMessage()
                                                            + " : "
                                                            + o.getErrorStack()));

            failure.setField("error_type", errorType.name());
            failure.setField("error_message", o.getErrorMessage());
            context.output(errorOutputTag, failure);
        } else {
            collector.collect(o.getOutput());
        }
    }

    enum ErrorType {
        NOT_FOUND(RevisionNotFoundException.class),
        NETWORK_ERROR(IOException.class, UncheckedIOException.class),
        MW_ERROR(InvalidMWApiResponseException.class);

        private final List<Class<? extends Throwable>> exceptionTypes;

        @SafeVarargs
        ErrorType(Class<? extends Throwable>... exceptionType) {
            this.exceptionTypes = copyOf(exceptionType);
        }

        public boolean matches(String exceptionType) {
            return exceptionTypes.stream().anyMatch(e -> e.getName().equals(exceptionType));
        }
    }
}

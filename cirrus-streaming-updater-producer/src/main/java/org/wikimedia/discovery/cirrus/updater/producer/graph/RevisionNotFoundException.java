package org.wikimedia.discovery.cirrus.updater.producer.graph;

import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusDocFetchException;

public class RevisionNotFoundException extends CirrusDocFetchException {
    public RevisionNotFoundException(String message) {
        super(message);
    }
}

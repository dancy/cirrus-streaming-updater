package org.wikimedia.discovery.cirrus.updater.producer.model;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INSTANT_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.LONG_TYPE_INFO;
import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.STRING_TYPE_INFO;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.ListTypeInfo;
import org.apache.flink.api.java.typeutils.MapTypeInfo;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.types.Row;
import org.wikimedia.discovery.cirrus.updater.common.model.UpdateFields;
import org.wikimedia.eventutilities.flink.EventRowTypeInfo;

import com.google.common.base.Verify;
import com.google.common.collect.ImmutableList;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class InputEventTypeInfo {

    public static TypeInformation<InputEvent> create(EventRowTypeInfo updateRowTypeInfo) {
        return new PojoTypeInfo<>(
                InputEvent.class, getFields(updateRowTypeInfo.getTypeAt(UpdateFields.FIELDS)));
    }

    private static List<PojoField> getFields(TypeInformation<Row> fieldsTypeInfo) {
        try {
            final Field changeType = InputEvent.class.getDeclaredField("changeType");
            final Field requestId = InputEvent.class.getDeclaredField("requestId");
            final Field eventStream = InputEvent.class.getDeclaredField("eventStream");
            final Field eventTime = InputEvent.class.getDeclaredField("eventTime");
            final Field revId = InputEvent.class.getDeclaredField("revId");
            final Field ingestionTime = InputEvent.class.getDeclaredField("ingestionTime");
            final Field targetDocument = InputEvent.class.getDeclaredField("targetDocument");
            final Field update = InputEvent.class.getDeclaredField("update");
            TypeInformation<InputEvent.TargetDocument> targetDocumentTypeInformation =
                    TypeInformation.of(InputEvent.TargetDocument.class);
            Verify.verify(
                    targetDocumentTypeInformation instanceof PojoTypeInfo,
                    InputEvent.TargetDocument.class
                            + "must be treated as a PojoTypeInfo, got "
                            + targetDocumentTypeInformation);
            return ImmutableList.of(
                    new PojoField(changeType, TypeInformation.of(ChangeType.class)),
                    new PojoField(requestId, STRING_TYPE_INFO),
                    new PojoField(eventStream, STRING_TYPE_INFO),
                    new PojoField(eventTime, INSTANT_TYPE_INFO),
                    new PojoField(revId, LONG_TYPE_INFO),
                    new PojoField(ingestionTime, INSTANT_TYPE_INFO),
                    new PojoField(targetDocument, targetDocumentTypeInformation),
                    new PojoField(update, getUpdateTypeInfo(fieldsTypeInfo)));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Failed to build type information for " + InputEvent.class, e);
        }
    }

    private static TypeInformation<InputEvent.Update> getUpdateTypeInfo(
            TypeInformation<Row> fieldsType) throws NoSuchFieldException {
        final Field noopHints = InputEvent.Update.class.getDeclaredField("noopHints");
        final Field rawFields = InputEvent.Update.class.getDeclaredField("rawFields");
        final Field weightedTags = InputEvent.Update.class.getDeclaredField("weightedTags");
        final Field redirectAdd = InputEvent.Update.class.getDeclaredField("redirectAdd");
        final Field redirectRemove = InputEvent.Update.class.getDeclaredField("redirectRemove");
        return new PojoTypeInfo<>(
                InputEvent.Update.class,
                ImmutableList.of(
                        new PojoField(noopHints, new MapTypeInfo<>(STRING_TYPE_INFO, STRING_TYPE_INFO)),
                        new PojoField(rawFields, fieldsType),
                        new PojoField(weightedTags, new ListTypeInfo<>(STRING_TYPE_INFO)),
                        new PojoField(redirectAdd, new ListTypeInfo<>(STRING_TYPE_INFO)),
                        new PojoField(redirectRemove, new ListTypeInfo<>(STRING_TYPE_INFO))));
    }
}

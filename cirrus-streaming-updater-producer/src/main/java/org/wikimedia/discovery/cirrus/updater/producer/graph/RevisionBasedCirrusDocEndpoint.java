package org.wikimedia.discovery.cirrus.updater.producer.graph;

import java.net.URI;

import org.wikimedia.discovery.cirrus.updater.common.SerializableBiFunction;
import org.wikimedia.discovery.cirrus.updater.common.graph.CirrusDocEndpoint;
import org.wikimedia.discovery.cirrus.updater.producer.model.InputEvent;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.BooleanNode;
import com.google.common.base.Preconditions;

public class RevisionBasedCirrusDocEndpoint implements CirrusDocEndpoint<InputEvent, InputEvent> {
    private static final String MW_API_PATH_AND_QUERY =
            "/w/api.php?action=query&format=json&cbbuilders=content&prop=cirrusbuilddoc&formatversion=2&format=json&revids=";
    private final SerializableBiFunction<InputEvent, JsonNode, InputEvent> augmenter;

    public RevisionBasedCirrusDocEndpoint(
            SerializableBiFunction<InputEvent, JsonNode, InputEvent> augmenter) {
        this.augmenter = augmenter;
    }

    @Override
    public URI buildURI(InputEvent inputEvent) {
        Preconditions.checkArgument(
                inputEvent.getRevId() != null, "inputEvent.getRevId() must not be null");
        return URI.create(
                "https://"
                        + inputEvent.getTargetDocument().getDomain()
                        + MW_API_PATH_AND_QUERY
                        + inputEvent.getRevId());
    }

    @Override
    public InputEvent extractAndAugment(InputEvent inputEvent, JsonNode response) {
        Preconditions.checkArgument(
                inputEvent.getRevId() != null, "inputEvent.getRevId() must not be null");
        final JsonNode missingFlagNode =
                response.at("/query/badrevids/" + inputEvent.getRevId() + "/missing");
        final JsonNode pageNode = response.at("/query/pages/0");
        if (missingFlagNode instanceof BooleanNode && missingFlagNode.asBoolean()
                || pageNode.isMissingNode()) {
            throw new RevisionNotFoundException("Revision " + inputEvent.getRevId() + " is missing");
        }
        return augmenter.apply(inputEvent, pageNode);
    }
}

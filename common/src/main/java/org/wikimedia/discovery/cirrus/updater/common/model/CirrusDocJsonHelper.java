package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.wikimedia.discovery.cirrus.updater.common.model.JsonPathUtils.getRequiredNode;

import java.util.Map;

import org.apache.flink.types.Row;
import org.wikimedia.eventutilities.flink.formats.json.JsonRowDeserializationSchema;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CirrusDocJsonHelper {
    private final JsonRowDeserializationSchema fieldsSchema;
    private final ObjectMapper objectMapper;
    private final JsonNode cirrusDoc;
    private final JsonNode cirrusDocMetadata;

    public CirrusDocJsonHelper(
            JsonRowDeserializationSchema fieldsSchema, ObjectMapper mapper, JsonNode root) {
        this(
                fieldsSchema,
                mapper,
                getRequiredNode(root, "/cirrusbuilddoc"),
                getRequiredNode(root, "/cirrusbuilddoc_metadata"));
    }

    public long getRevId() {
        return getRequiredNode(cirrusDoc, "/version").asLong();
    }

    public long getPageId() {
        return getRequiredNode(cirrusDoc, "/page_id").asLong();
    }

    public long getNamespace() {
        return getRequiredNode(cirrusDoc, "/namespace").asLong();
    }

    public String getClusterGroup() {
        return getRequiredNode(cirrusDocMetadata, "/cluster_group").asText();
    }

    public String getIndexName() {
        return getRequiredNode(cirrusDocMetadata, "/index_name").asText();
    }

    public Map<String, String> getNoopHints() {
        return objectMapper.convertValue(
                getRequiredNode(cirrusDocMetadata, "/noop_hints"),
                new TypeReference<Map<String, String>>() {});
    }

    public Row getRawFields() {
        return fieldsSchema.convert(cirrusDoc);
    }
}

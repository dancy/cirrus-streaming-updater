package org.wikimedia.discovery.cirrus.updater.common.graph;

/** Exception thrown while fetching the cirrus doc out of the MW API. */
public abstract class CirrusDocFetchException extends RuntimeException {
    public CirrusDocFetchException(String s) {
        super(s);
    }
}

package org.wikimedia.discovery.cirrus.updater.common.http;

import java.time.Duration;
import java.util.Map;

import org.apache.hc.client5.http.config.RequestConfig;
import org.apache.hc.client5.http.cookie.StandardCookieSpec;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.DefaultSchemePortResolver;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.client5.http.impl.async.HttpAsyncClients;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManager;
import org.apache.hc.client5.http.impl.io.PoolingHttpClientConnectionManagerBuilder;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManager;
import org.apache.hc.client5.http.impl.nio.PoolingAsyncClientConnectionManagerBuilder;
import org.apache.hc.client5.http.impl.routing.DefaultRoutePlanner;
import org.apache.hc.client5.http.routing.HttpRoutePlanner;
import org.apache.hc.client5.http.ssl.ClientTlsStrategyBuilder;
import org.apache.hc.core5.http.ssl.TLS;
import org.apache.hc.core5.http2.HttpVersionPolicy;
import org.apache.hc.core5.pool.PoolConcurrencyPolicy;
import org.apache.hc.core5.pool.PoolReusePolicy;
import org.apache.hc.core5.ssl.SSLContexts;
import org.apache.hc.core5.util.TimeValue;
import org.apache.hc.core5.util.Timeout;

import lombok.experimental.UtilityClass;

@UtilityClass
@SuppressWarnings("HideUtilityClassConstructor")
public class HttpClientFactory {
    public static CloseableHttpAsyncClient buildAsyncClient(
            Map<String, String> httpRoutes, Duration requestTimeout) {
        final PoolingAsyncClientConnectionManager connectionManager =
                PoolingAsyncClientConnectionManagerBuilder.create()
                        .setTlsStrategy(
                                ClientTlsStrategyBuilder.create()
                                        .setSslContext(SSLContexts.createSystemDefault())
                                        .setTlsVersions(TLS.V_1_3, TLS.V_1_2)
                                        .build())
                        .setPoolConcurrencyPolicy(PoolConcurrencyPolicy.STRICT)
                        .setConnPoolPolicy(PoolReusePolicy.LIFO)
                        .setConnectionTimeToLive(TimeValue.ofMinutes(1L))
                        .build();

        return HttpAsyncClients.custom()
                .setRetryStrategy(DefaultHttpRequestRetryStrategy.INSTANCE)
                .setConnectionManager(connectionManager)
                .setRoutePlanner(createHttpRoutePlanner(httpRoutes))
                .setDefaultRequestConfig(
                        RequestConfig.custom()
                                .setConnectTimeout(Timeout.ofMilliseconds(requestTimeout.toMillis()))
                                .setResponseTimeout(Timeout.ofMilliseconds(requestTimeout.toMillis()))
                                .setCookieSpec(StandardCookieSpec.IGNORE)
                                .build())
                .setVersionPolicy(HttpVersionPolicy.NEGOTIATE)
                .build();
    }

    public static CloseableHttpClient buildSyncClient(
            Map<String, String> httpRoutes, Duration requestTimeout) {
        final PoolingHttpClientConnectionManager connectionManager =
                PoolingHttpClientConnectionManagerBuilder.create()
                        .setPoolConcurrencyPolicy(PoolConcurrencyPolicy.STRICT)
                        .setConnPoolPolicy(PoolReusePolicy.LIFO)
                        .setConnectionTimeToLive(TimeValue.ofMinutes(1L))
                        .build();

        return HttpClients.custom()
                // it's up to the caller to handle retries
                .setRetryStrategy(new DefaultHttpRequestRetryStrategy(0, TimeValue.ZERO_MILLISECONDS))
                .setConnectionManager(connectionManager)
                .setRoutePlanner(createHttpRoutePlanner(httpRoutes))
                .setDefaultRequestConfig(
                        RequestConfig.custom()
                                .setConnectTimeout(Timeout.ofMilliseconds(requestTimeout.toMillis()))
                                .setResponseTimeout(Timeout.ofMilliseconds(requestTimeout.toMillis()))
                                .setCookieSpec(StandardCookieSpec.IGNORE)
                                .build())
                .build();
    }

    private static HttpRoutePlanner createHttpRoutePlanner(Map<String, String> httpRoutes) {
        HttpRoutePlanner routePlanner = new DefaultRoutePlanner(DefaultSchemePortResolver.INSTANCE);
        if (httpRoutes != null) {
            routePlanner = new CustomRoutePlanner(CustomRoutePlanner.map(httpRoutes), routePlanner);
        }
        return routePlanner;
    }
}

package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.Closeable;
import java.io.IOException;
import java.io.Serializable;
import java.time.Duration;
import java.util.Map;
import java.util.concurrent.CancellationException;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import org.apache.hc.client5.http.async.methods.SimpleHttpRequest;
import org.apache.hc.client5.http.async.methods.SimpleHttpResponse;
import org.apache.hc.client5.http.impl.DefaultHttpRequestRetryStrategy;
import org.apache.hc.client5.http.impl.async.CloseableHttpAsyncClient;
import org.apache.hc.core5.concurrent.FutureCallback;
import org.apache.hc.core5.http.Method;
import org.wikimedia.discovery.cirrus.updater.common.http.HttpClientFactory;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;

/**
 * A {@link Function function} fetching revision data via HTTP.
 *
 * <p>This function only retries HTTP requests if protocol implies it, for example by responding
 * with a code of <a href="https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/429">429</a>,
 * see {@link DefaultHttpRequestRetryStrategy}. Retries implied by the HTTP payload must be
 * implemented at a higher level, for example via {@link RetryingAsyncFunction}. Otherwise, we would
 * have to tell the content-agnostic part of the HTTP stack how to interpret content.
 *
 * @param <I> the type of input event
 * @param <O> the type of output event
 */
@SuppressFBWarnings("SE_NO_SERIALVERSIONID")
public class CirrusDocFetcher<I, O>
        implements Function<I, CompletionStage<O>>, Serializable, Closeable {

    private final Duration requestTimeout;

    private final CirrusDocEndpoint<I, O> endpoint;
    private final Map<String, String> httpRoutes;

    private transient CloseableHttpAsyncClient httpClient;

    private transient ObjectMapper objectMapper;

    public CirrusDocFetcher(
            Duration requestTimeout, CirrusDocEndpoint<I, O> endpoint, Map<String, String> httpRoutes) {
        this.requestTimeout = requestTimeout;
        this.endpoint = endpoint;
        this.httpRoutes = httpRoutes;
    }

    @Override
    public void close() throws IOException {
        if (httpClient != null) {
            httpClient.close();
        }
    }

    @Override
    public CompletionStage<O> apply(I inputEvent) {
        final CompletableFuture<O> result = new CompletableFuture<>();

        getClient()
                .execute(
                        SimpleHttpRequest.create(Method.GET.name(), endpoint.buildURI(inputEvent)),
                        new ResponseCallback<>(endpoint, inputEvent, getObjectMapper(), result));

        return result;
    }

    private CloseableHttpAsyncClient getClient() {
        if (httpClient == null) {
            httpClient = HttpClientFactory.buildAsyncClient(httpRoutes, requestTimeout);
            httpClient.start();
        }

        return httpClient;
    }

    private ObjectMapper getObjectMapper() {
        if (objectMapper == null) {
            objectMapper = new ObjectMapper();
        }
        return objectMapper;
    }

    private static final class ResponseCallback<I, O> implements FutureCallback<SimpleHttpResponse> {

        private final CirrusDocEndpoint<I, O> endpoint;
        private final I inputEvent;

        private final ObjectMapper objectMapper;

        private final CompletableFuture<O> delegate;

        ResponseCallback(
                CirrusDocEndpoint<I, O> endpoint,
                I inputEvent,
                ObjectMapper objectMapper,
                CompletableFuture<O> delegate) {

            this.endpoint = endpoint;
            this.inputEvent = inputEvent;
            this.objectMapper = objectMapper;
            this.delegate = delegate;
        }

        @Override
        @SuppressWarnings("IllegalCatch")
        public void completed(SimpleHttpResponse httpResponse) {
            // Based on
            // https://github.com/openjdk-mirror/jdk7u-jdk/blob/master/src/share/classes/java/util/concurrent/ThreadPoolExecutor.java#L1109
            // goal is to avoid leaking an exception as we want to notify the delegate that something did
            // not run well.
            // We also want to stop the current thread in case an unexpected problem occurred.
            Throwable thrown = null;
            try {
                delegate.complete(readResponse(httpResponse));
            } catch (IOException | CirrusDocFetchException e) {
                // these are the exception we expect, only notify the delegate
                thrown = e;
            } catch (Throwable e) {
                // every other problems is both propagated to the current thread and the delegate
                thrown = e;
                throw e;
            } finally {
                if (thrown != null) {
                    delegate.completeExceptionally(thrown);
                }
            }
        }

        @Override
        public void failed(Exception e) {
            delegate.completeExceptionally(e);
        }

        @Override
        public void cancelled() {
            failed(new CancellationException());
        }

        private O readResponse(SimpleHttpResponse httpResponse) throws IOException {
            final byte[] json = httpResponse.getBody().getBodyBytes();
            if (httpResponse.getCode() != 200) {
                throw new InvalidMWApiResponseException(
                        "Unexpected response ("
                                + httpResponse.getCode()
                                + ") for revision: "
                                + httpResponse.getReasonPhrase());
            }

            final JsonNode rootNode = objectMapper.readTree(json);
            return endpoint.extractAndAugment(inputEvent, rootNode);
        }
    }
}

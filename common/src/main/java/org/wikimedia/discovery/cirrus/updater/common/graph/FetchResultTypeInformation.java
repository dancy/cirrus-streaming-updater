package org.wikimedia.discovery.cirrus.updater.common.graph;

import static com.google.common.collect.ImmutableList.of;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.flink.api.common.typeinfo.BasicTypeInfo;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.PojoField;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;

public final class FetchResultTypeInformation {

    private FetchResultTypeInformation() {}

    public static <I, O> PojoTypeInfo<FetchResult<I, O>> create(
            TypeInformation<I> inputTypeInfo, TypeInformation<O> outputTypeInfo) {
        return new PojoTypeInfo<>(
                new TypeHint<FetchResult<I, O>>() {}.getTypeInfo().getTypeClass(),
                getFields(inputTypeInfo, outputTypeInfo));
    }

    private static <I, O> List<PojoField> getFields(
            TypeInformation<I> inputTypeInfo, TypeInformation<O> outputTypeInfo) {
        try {
            final Field input = FetchResult.class.getDeclaredField("input");
            final Field output = FetchResult.class.getDeclaredField("output");
            final Field errorType = FetchResult.class.getDeclaredField("errorType");
            final Field errorMessage = FetchResult.class.getDeclaredField("errorMessage");
            final Field errorStack = FetchResult.class.getDeclaredField("errorStack");
            return of(
                    new PojoField(input, inputTypeInfo),
                    new PojoField(output, outputTypeInfo),
                    new PojoField(errorType, BasicTypeInfo.STRING_TYPE_INFO),
                    new PojoField(errorMessage, BasicTypeInfo.STRING_TYPE_INFO),
                    new PojoField(errorStack, BasicTypeInfo.STRING_TYPE_INFO));
        } catch (NoSuchFieldException e) {
            throw new IllegalStateException(
                    "Failed to build type information for " + FetchResult.class, e);
        }
    }
}

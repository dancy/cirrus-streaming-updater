package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.util.Objects;

import org.apache.flink.util.ExceptionUtils;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public final class FetchResult<I, O> {
    I input;
    O output;
    String errorType;
    String errorMessage;
    String errorStack;

    private FetchResult(I input, String errorType, String errorMessage, String errorStack) {
        this.input = input;
        this.errorType = errorType;
        this.errorMessage = errorMessage;
        this.errorStack = errorStack;
    }

    private FetchResult(O output) {
        this.output = output;
    }

    public boolean hasError() {
        return output == null;
    }

    public static <I, O> FetchResult<I, O> fromError(I event, Throwable error) {
        Throwable t = Objects.requireNonNull(error);
        return new FetchResult<I, O>(
                Objects.requireNonNull(event),
                t.getClass().getName(),
                t.getMessage(),
                ExceptionUtils.stringifyException(t));
    }

    public static <I, O> FetchResult<I, O> success(O event) {
        return new FetchResult<>(Objects.requireNonNull(event));
    }
}

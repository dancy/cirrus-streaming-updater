package org.wikimedia.discovery.cirrus.updater.common.graph;

import java.io.Serializable;
import java.net.URI;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Define an endpoint to fetch the CirrusSearch doc.
 *
 * @param <I> The type of input event for this endpoint
 * @param <O> The type of output emitted form this endpoint
 */
public interface CirrusDocEndpoint<I, O> extends Serializable {

    URI buildURI(I inputEvent);

    O extractAndAugment(I inputEvent, JsonNode response);
}

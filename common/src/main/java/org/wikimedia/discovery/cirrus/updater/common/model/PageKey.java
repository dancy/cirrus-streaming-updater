package org.wikimedia.discovery.cirrus.updater.common.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageKey {
    @NonNull String wiki;
    long pageId;
}

package org.wikimedia.discovery.cirrus.updater.common.graph;

import static java.util.Collections.singletonList;

import java.io.Closeable;
import java.io.Serializable;
import java.time.Duration;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CompletionException;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.flink.streaming.api.functions.async.AsyncFunction;
import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.apache.flink.streaming.api.functions.async.RichAsyncFunction;

import edu.umd.cs.findbugs.annotations.SuppressFBWarnings;
import lombok.extern.slf4j.Slf4j;

/**
 * An {@link AsyncFunction} that repeats calling its delegate {@code F(I) -> CompletionStage<O>}
 * while predicate {@code P(O)} is {@code true}.
 *
 * @param <I> type of input
 * @param <O> type of output
 * @param <F> interface of function mapping input {@link I} to output {@link O} (asynchronously)
 * @param <P> interface of predicate indicating retry based on input and output of {@link F}
 */
@Slf4j
@SuppressFBWarnings(
        value = "IMC_IMMATURE_CLASS_BAD_SERIALVERSIONUID",
        justification = "generated serialVersionUID is considered invalid")
public class RetryingAsyncFunction<
                I,
                O,
                F extends Function<I, CompletionStage<O>> & Serializable,
                P extends Predicate<RetryContext<I, O>> & Serializable>
        extends RichAsyncFunction<I, FetchResult<I, O>> {

    private static final long serialVersionUID = 1712550275409981580L;

    private final F delegate;

    private final P predicate;

    private transient Timer delayTimer;

    public RetryingAsyncFunction(F delegate, P predicate) {
        this.delegate = delegate;
        this.predicate = predicate;
    }

    @Override
    public void close() throws Exception {
        super.close();

        if (delegate instanceof Closeable) {
            ((Closeable) delegate).close();
        }
    }

    @Override
    public void asyncInvoke(I input, ResultFuture<FetchResult<I, O>> resultFuture) {
        asyncInvokeWithRetry(input, resultFuture, 0);
    }

    public Duration calculateDelay(int iteration) {
        return Duration.ofMillis(1000L * iteration);
    }

    private void asyncInvokeWithRetry(
            I input, ResultFuture<FetchResult<I, O>> resultFuture, int iteration) {

        delegate
                .apply(input)
                .whenComplete(
                        (output, error) ->
                                completeOrRetry(
                                        resultFuture, RetryContext.of(iteration, input, output, unwrapError(error))));
    }

    private static Throwable unwrapError(Throwable error) {
        return error instanceof CompletionException ? error.getCause() : error;
    }

    private void completeOrRetry(
            ResultFuture<FetchResult<I, O>> resultFuture, RetryContext<I, O> context) {

        if (context.getFailure().isPresent()) {
            Throwable failure = context.getFailure().get();
            if (failure instanceof CompletionException) {
                failure = failure.getCause();
            }

            if (predicate.test(context)) {
                log.debug(
                        "RETRYING ({}): {} {}",
                        context.getIteration(),
                        context.getInput(),
                        failure.getMessage());
                retry(context.getInput(), resultFuture, context.getIteration());
            } else {
                resultFuture.complete(singletonList(FetchResult.fromError(context.getInput(), failure)));
            }
        } else {
            resultFuture.complete(
                    singletonList(
                            FetchResult.success(
                                    context
                                            .getOutput()
                                            .orElseThrow(() -> new IllegalArgumentException("Expected an output")))));
        }
    }

    private void retry(I input, ResultFuture<FetchResult<I, O>> resultFuture, int iteration) {
        final TimerTask task =
                new TimerTask() {
                    @Override
                    public void run() {
                        asyncInvokeWithRetry(input, resultFuture, iteration + 1);
                    }
                };

        getTimer().schedule(task, calculateDelay(iteration).toMillis());
    }

    private Timer getTimer() {
        if (delayTimer == null) {
            delayTimer = new Timer("delayed-retry", true);
        }
        return delayTimer;
    }
}

package org.wikimedia.discovery.cirrus.updater.common.async;

import java.time.Duration;
import java.util.concurrent.CompletionStage;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class CompletableFutureBackPortsTest {

    @Test
    void completedFuture() {
        final Object value = new Object();
        final CompletionStage<Object> completionStage =
                CompletableFutureBackPorts.completedFuture(value);

        Assertions.assertThat(completionStage).isCompletedWithValue(value);
    }

    @Test
    void failedFuture() {
        final String message = "Failed at " + System.currentTimeMillis();
        final Exception exception = new Exception(message);
        final CompletionStage<Object> completionStage =
                CompletableFutureBackPorts.failedFuture(exception);

        Assertions.assertThat(completionStage)
                .failsWithin(Duration.ZERO)
                .withThrowableOfType(Exception.class)
                .withMessageEndingWith(message);
    }
}

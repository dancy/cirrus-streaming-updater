package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatNoException;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.wikimedia.discovery.cirrus.updater.common.wiremock.WireMockExtensionUtilities.getWireMockExtension;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.hc.core5.http.HttpHeaders;
import org.apache.hc.core5.http.HttpStatus;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.mockito.junit.jupiter.MockitoExtension;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.http.Fault;
import com.github.tomakehurst.wiremock.junit5.WireMockExtension;
import com.github.tomakehurst.wiremock.junit5.WireMockRuntimeInfo;
import com.google.common.collect.ImmutableMap;

@ExtendWith(MockitoExtension.class)
class CirrusDocFetcherTest {
    static final CirrusDocEndpoint<Long, Long> FAKE_ENDPOINT =
            new CirrusDocEndpoint<Long, Long>() {
                @Override
                public URI buildURI(Long inputRevision) {
                    return URI.create(
                            "https://"
                                    + DOMAIN
                                    + "/w/api.php?action=query&format=json&cbbuilders=content"
                                    + "&prop=cirrusbuilddoc&formatversion=2&format=json&revids="
                                    + inputRevision);
                }

                @Override
                public Long extractAndAugment(Long inputEvent, JsonNode response) {
                    return response.at("/query/pages/0/cirrusbuilddoc/version").asLong();
                }
            };

    @RegisterExtension static WireMockExtension wireMockExtension = getWireMockExtension();

    static final String DOMAIN = "mydomain.local";

    CirrusDocFetcher<Long, Long> revisionFetcher;

    @BeforeEach
    void createFetcher(WireMockRuntimeInfo info) {
        revisionFetcher =
                new CirrusDocFetcher<>(
                        Duration.ofMillis(250), FAKE_ENDPOINT, ImmutableMap.of(DOMAIN, info.getHttpBaseUrl()));
    }

    @AfterEach
    void closeFetcher() throws IOException {
        if (revisionFetcher != null) {
            revisionFetcher.close();
        }
    }

    @Test
    void properResponses() {
        final long revId = 551141L;

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(
                                WireMock.aResponse()
                                        .withBodyFile("cirrus_build_doc_test.revision." + revId + ".json")));

        assertThatNoException()
                .isThrownBy(
                        () -> {
                            assertThat(get(revisionFetcher, revId)).isEqualTo(revId);
                        });
    }

    @Test
    void faultyResponses() {
        final long revId = 551142L;

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .willReturn(WireMock.serverError().withFixedDelay(1000))
                        .willSetStateTo("too many requests"));

        assertThatThrownBy(() -> get(revisionFetcher, revId))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(IOException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("too many requests")
                        .willReturn(
                                WireMock.status(HttpStatus.SC_TOO_MANY_REQUESTS)
                                        .withHeader(HttpHeaders.RETRY_AFTER, "2")
                                        .withFixedDelay(50))
                        .willSetStateTo("server error"));

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("server error")
                        .willReturn(WireMock.serverError().withFixedDelay(50))
                        .willSetStateTo("connection reset"));

        assertThatThrownBy(() -> get(revisionFetcher, revId))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(InvalidMWApiResponseException.class);

        wireMockExtension.stubFor(
                WireMock.get(WireMock.urlMatching("/w/api.php.*revids=" + revId + ".*"))
                        .inScenario(Long.toString(revId))
                        .whenScenarioStateIs("connection reset")
                        .willReturn(WireMock.serverError().withFault(Fault.CONNECTION_RESET_BY_PEER)));

        assertThatThrownBy(() -> get(revisionFetcher, revId))
                .isInstanceOf(ExecutionException.class)
                .cause()
                .isInstanceOf(IOException.class);
    }

    private static Long get(CirrusDocFetcher<Long, Long> revisionFetcher, Long revision)
            throws ExecutionException, InterruptedException, TimeoutException {
        return revisionFetcher.apply(revision).toCompletableFuture().get(5, TimeUnit.SECONDS);
    }
}

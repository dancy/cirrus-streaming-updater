package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.apache.flink.api.common.typeinfo.BasicTypeInfo.INT_TYPE_INFO;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.util.stream.Stream;

import org.apache.flink.api.common.ExecutionConfig;
import org.apache.flink.api.common.typeutils.TypeSerializer;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.apache.flink.api.java.typeutils.runtime.TestDataOutputSerializer;
import org.apache.flink.core.memory.DataInputDeserializer;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

class FetchResultTypeInformationTest {

    public static Stream<Arguments> provideCases() {
        return Stream.of(
                Arguments.of(FetchResult.fromError(1, new Exception("boom"))),
                Arguments.of(FetchResult.success(1)));
    }

    @ParameterizedTest
    @MethodSource("provideCases")
    void serde(FetchResult<Integer, Integer> result) throws IOException {
        final PojoTypeInfo<FetchResult<Integer, Integer>> typeInformation =
                FetchResultTypeInformation.create(INT_TYPE_INFO, INT_TYPE_INFO);

        ExecutionConfig config = new ExecutionConfig();
        config.disableGenericTypes();
        final TypeSerializer<FetchResult<Integer, Integer>> serializer =
                typeInformation.createSerializer(config);

        final TestDataOutputSerializer target = new TestDataOutputSerializer(4096);

        serializer.serialize(result, target);

        final FetchResult<Integer, Integer> restoredContext =
                serializer.deserialize(new DataInputDeserializer(target.copyByteBuffer()));

        assertThat(result.getInput()).isEqualTo(restoredContext.getInput());
        assertThat(result.getOutput()).isEqualTo(restoredContext.getOutput());
        assertThat(result.getErrorType()).isEqualTo(restoredContext.getErrorType());
        assertThat(result.getErrorMessage()).isEqualTo(restoredContext.getErrorMessage());
        assertThat(result.getErrorStack()).isEqualTo(restoredContext.getErrorStack());
    }
}

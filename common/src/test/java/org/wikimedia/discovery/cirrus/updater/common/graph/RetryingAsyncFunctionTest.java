package org.wikimedia.discovery.cirrus.updater.common.graph;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import java.io.Serializable;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;
import java.util.function.Predicate;

import org.apache.flink.streaming.api.functions.async.ResultFuture;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.wikimedia.discovery.cirrus.updater.common.async.CompletableFutureBackPorts;

@ExtendWith(MockitoExtension.class)
class RetryingAsyncFunctionTest {

    final CompletableResultFuture<FetchResult<Input, Output>> outputHolder =
            new CompletableResultFuture<>();

    final Input input = new Input();

    @Mock(serializable = true)
    Delegate<Input, CompletionStage<Output>> delegate;

    @Spy
    SerializablePredicate<RetryContext<Input, Output>> predicate =
            new SerializablePredicate<RetryContext<Input, Output>>() {

                private static final long serialVersionUID = 1;

                int retries = 2;

                @Override
                public boolean test(RetryContext<Input, Output> context) {
                    return --retries >= 0
                            && (context.getFailure().map(RetryableException.class::isInstance).orElse(false));
                }
            };

    RetryingAsyncFunction<
                    Input,
                    Output,
                    Delegate<Input, CompletionStage<Output>>,
                    SerializablePredicate<RetryContext<Input, Output>>>
            retryingAsyncFunction;

    @BeforeEach
    void setup() {
        retryingAsyncFunction = new RetryingAsyncFunction<>(delegate, predicate);
    }

    @Test
    void completesWithoutRetry() throws ExecutionException, InterruptedException, TimeoutException {
        final Output output = new Output();
        Mockito.when(delegate.apply(Mockito.any()))
                .thenReturn(CompletableFutureBackPorts.completedFuture(output));

        final Input input = new Input();
        final CompletableResultFuture<FetchResult<Input, Output>> completableResultFuture =
                new CompletableResultFuture<>();
        retryingAsyncFunction.asyncInvoke(input, completableResultFuture);

        final FetchResult<Input, Output> tuple =
                completableResultFuture.completableFuture.get(5, TimeUnit.SECONDS);

        assertThat(tuple.getOutput()).isSameAs(output);
        assertThat(tuple.hasError()).isFalse();

        Mockito.verify(predicate, Mockito.times(0)).test(Mockito.any());
        Mockito.verify(delegate, Mockito.times(1)).apply(input);
    }

    @Test
    void retriesOnRetryableException()
            throws ExecutionException, InterruptedException, TimeoutException {
        final RetryableException exception = new RetryableException();
        Mockito.when(delegate.apply(Mockito.any()))
                .thenReturn(CompletableFutureBackPorts.failedFuture(exception));

        final Input input = new Input();
        final CompletableResultFuture<FetchResult<Input, Output>> completableResultFuture =
                new CompletableResultFuture<>();

        retryingAsyncFunction.asyncInvoke(input, completableResultFuture);

        final FetchResult<Input, Output> tuple =
                completableResultFuture.completableFuture.get(5, TimeUnit.SECONDS);

        assertThat(tuple.getInput()).isEqualTo(input);
        assertThat(tuple.getOutput()).isNull();
        assertThat(tuple.hasError()).isTrue();
        assertThat(tuple.getErrorType()).isEqualTo(RetryableException.class.getName());

        Mockito.verify(predicate, Mockito.times(3)).test(Mockito.any());
        Mockito.verify(delegate, Mockito.times(3)).apply(input);
    }

    @Test
    void failsFastOnNonRetryableException()
            throws ExecutionException, InterruptedException, TimeoutException {
        final OutOfMemoryError exception = new OutOfMemoryError();
        Mockito.when(delegate.apply(Mockito.any()))
                .thenReturn(CompletableFutureBackPorts.failedFuture(exception));

        final Input input = new Input();
        final CompletableResultFuture<FetchResult<Input, Output>> completableResultFuture =
                new CompletableResultFuture<>();

        retryingAsyncFunction.asyncInvoke(input, completableResultFuture);

        final FetchResult<Input, Output> tuple =
                completableResultFuture.completableFuture.get(5, TimeUnit.SECONDS);

        assertThat(tuple.getInput()).isEqualTo(input);
        assertThat(tuple.getOutput()).isNull();
        assertThat(tuple.hasError()).isTrue();
        assertThat(tuple.getErrorType()).isEqualTo(OutOfMemoryError.class.getName());

        Mockito.verify(predicate, Mockito.times(1)).test(Mockito.any());
        Mockito.verify(delegate, Mockito.times(1)).apply(input);
    }

    @Test
    @SuppressWarnings("unchecked")
    void completesAfterMultipleRetries()
            throws ExecutionException, InterruptedException, TimeoutException {
        final RetryableException exception = new RetryableException();
        final Output output = new Output();
        Mockito.when(delegate.apply(Mockito.any()))
                .thenReturn(
                        CompletableFutureBackPorts.failedFuture(exception),
                        CompletableFuture.completedFuture(output));

        retryingAsyncFunction.asyncInvoke(input, outputHolder);

        final FetchResult<Input, Output> tuple =
                outputHolder.completableFuture.get(5, TimeUnit.SECONDS);

        assertThat(tuple.getOutput()).isSameAs(output);
        assertThat(tuple.hasError()).isFalse();

        Mockito.verify(predicate, Mockito.times(1)).test(Mockito.any());
        Mockito.verify(delegate, Mockito.times(2)).apply(input);
    }

    static class RetryableException extends Exception {

        private static final long serialVersionUID = 1;
    }

    static class Input implements Serializable {

        private static final long serialVersionUID = 2740077103088039162L;
    }

    static class Output implements Serializable {

        private static final long serialVersionUID = -2842112367899471933L;
    }

    interface Delegate<IN, OUT> extends Function<IN, OUT>, Serializable {}

    interface SerializablePredicate<ARG> extends Predicate<ARG>, Serializable {}

    /**
     * {@link ResultFuture} implementation based on {@link CompletableFuture}. Allows us to wait for
     * an async result without the need for an additional library.
     *
     * @param <OUT> output type
     */
    static class CompletableResultFuture<OUT> implements ResultFuture<OUT> {

        final CompletableFuture<OUT> completableFuture;

        CompletableResultFuture() {
            this(new CompletableFuture<>());
        }

        CompletableResultFuture(CompletableFuture<OUT> completableFuture) {
            this.completableFuture = completableFuture;
        }

        @Override
        public void complete(Collection<OUT> result) {
            result.stream().findFirst().ifPresent(completableFuture::complete);
        }

        @Override
        public void completeExceptionally(Throwable error) {
            Assertions.fail("Retry should complete regularly and wrap any exception in OUT-type");
        }
    }
}

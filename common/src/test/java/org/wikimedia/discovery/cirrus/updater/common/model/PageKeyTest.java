package org.wikimedia.discovery.cirrus.updater.common.model;

import static org.assertj.core.api.Assertions.assertThat;

import org.apache.flink.api.common.typeinfo.TypeInformation;
import org.apache.flink.api.java.typeutils.PojoTypeInfo;
import org.junit.jupiter.api.Test;

class PageKeyTest {
    @Test
    void testIsPojoCompatible() {
        assertThat(TypeInformation.of(PageKey.class)).isInstanceOf(PojoTypeInfo.class);
    }
}

<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <parent>
        <groupId>org.wikimedia.discovery</groupId>
        <artifactId>discovery-parent-pom</artifactId>
        <version>1.66</version>
    </parent>
    <groupId>org.wikimedia.discovery.cirrus.updater</groupId>
    <artifactId>cirrus-streaming-updater-parent</artifactId>
    <version>0.1.0-SNAPSHOT</version>
    <packaging>pom</packaging>

    <name>CirrusSearch Streaming Updater</name>
    <url>https://gerrit.wikimedia.org/r/admin/repos/search/cirrus-streaming-updater</url>

    <licenses>
        <license>
            <name>The Apache Software License, Version 2.0</name>
            <url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
            <distribution>repo</distribution>
        </license>
    </licenses>

    <modules>
        <module>common</module>
        <module>cirrus-streaming-updater-consumer</module>
        <module>cirrus-streaming-updater-producer</module>
    </modules>

    <scm>
        <connection>scm:git:https://gerrit.wikimedia.org/r/search/cirrus-streaming-updater</connection>
        <developerConnection>scm:git:ssh://gerrit.wikimedia.org:29418/search/cirrus-streaming-updater</developerConnection>
        <tag>HEAD</tag>
        <url>https://gerrit.wikimedia.org/r/admin/repos/search/cirrus-streaming-updater</url>
    </scm>

    <properties>
        <flink.compat.version>1.16</flink.compat.version>
        <flink.version>1.16.0</flink.version>
        <httpclient.version>5.1.3</httpclient.version>
        <!-- The hadoop cluster we want to use for testing is stuck with Java 1.8 for now -->
        <maven.compiler.target>8</maven.compiler.target>
    </properties>

    <!--
    NOTE:
    - ALL dependencies and version should be managed in this parent pom
      or in discovery-parent-pom.

    - Module poms should list specific dependencies, without <version>.  This
      allows us to manage versions in one place.

    - Unless necessary, <scope> should not be specified in this parent pom.
      Module poms should specify scopes like test or provided.
      This allows us to not have to re-sort both poms if a dependency moves from
      e.g. scope test to provided, and also allows for different modules
      to use the same dependency in different scopes.

    -->
    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.github.ben-manes.caffeine</groupId>
                <artifactId>caffeine</artifactId>
                <version>2.9.3</version>
            </dependency>
            <dependency>
                <groupId>com.github.tomakehurst</groupId>
                <artifactId>wiremock-jre8</artifactId>
                <version>2.33.2</version>
            </dependency>
            <dependency>
                <groupId>io.findify</groupId>
                <artifactId>flink-scala-api_${scala.compat.version}</artifactId>
                <version>${flink.compat.version}-2</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-clients</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-connector-files</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-connector-kafka</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-java</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-json</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-runtime</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-streaming-java</artifactId>
                <version>${flink.version}</version>
            </dependency>

            <!-- eventhough we don't use tables it is required by wikimedia flink utilities -->
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-table-api-java</artifactId>
                <version>${flink.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.httpcomponents.client5</groupId>
                <artifactId>httpclient5</artifactId>
                <version>${httpclient.version}</version>
            </dependency>
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka-clients</artifactId>
                <version>3.2.3</version>
            </dependency>
            <dependency>
                <groupId>org.wikimedia</groupId>
                <artifactId>eventutilities-flink</artifactId>
                <version>1.2.12</version>
                <exclusions>
                    <exclusion>
                        <groupId>javax.activation</groupId>
                        <artifactId>activation</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.wikimedia.discovery.cirrus.updater</groupId>
                <artifactId>common</artifactId>
                <version>${project.version}</version>
            </dependency>
            <dependency>
                <groupId>net.javacrumbs.json-unit</groupId>
                <artifactId>json-unit-assertj</artifactId>
                <version>2.34.0</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-streaming-java</artifactId>
                <version>${flink.version}</version>
                <type>test-jar</type>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-test-utils</artifactId>
                <version>${flink.version}</version>
                <scope>test</scope>
                <exclusions>
                    <exclusion>
                        <groupId>log4j</groupId>
                        <artifactId>log4j</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.apache.flink</groupId>
                <artifactId>flink-test-utils-junit</artifactId>
                <version>${flink.version}</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.kafka</groupId>
                <artifactId>kafka_2.13</artifactId>
                <version>3.2.3</version>
                <scope>test</scope>
                <exclusions>
                    <!-- does not play well with json-unit-assertj -->
                    <exclusion>
                        <groupId>com.fasterxml.jackson.module</groupId>
                        <artifactId>jackson-module-scala_2.13</artifactId>
                    </exclusion>
                </exclusions>
            </dependency>
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-core</artifactId>
                <version>2.19.0</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.apache.logging.log4j</groupId>
                <artifactId>log4j-slf4j2-impl</artifactId>
                <version>2.19.0</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.junit.jupiter</groupId>
                <artifactId>junit-jupiter</artifactId>
                <!-- version is aligned with what Flink is using -->
                <version>5.8.1</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-junit-jupiter</artifactId>
                <version>4.8.0</version>
                <scope>test</scope>
            </dependency>
            <dependency>
                <groupId>org.wikimedia.discovery.cirrus.updater</groupId>
                <artifactId>common</artifactId>
                <version>${project.version}</version>
                <type>test-jar</type>
                <scope>test</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <build>
        <!--
        Use pluginManagment here to declare plugin defaults that aren't
        already covered by discovery-parent-pom.
        Then, enable those plugins in the <build><plugins>...
        section of your module pom.
        -->
        <pluginManagement>
            <plugins>
                <!-- We use the maven-assembly-plugin to create a fat jar that contains all necessary dependencies. -->
                <!-- Change the value of <mainClass>...</mainClass> if your program entry point changes. -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-assembly-plugin</artifactId>
                    <dependencies>
                        <dependency>
                            <groupId>org.wikimedia.discovery</groupId>
                            <artifactId>discovery-maven-tool-configs</artifactId>
                            <version>${discovery-maven-tool-configs.version}</version>
                        </dependency>
                    </dependencies>
                    <executions>
                        <execution>
                            <id>jar-with-dependencies</id>
                            <goals>
                                <goal>single</goal>
                            </goals>
                            <phase>package</phase>
                            <configuration>
                                <descriptorRefs>
                                    <descriptorRef>jar-with-dependencies-spi-compliant</descriptorRef>
                                </descriptorRefs>
                            </configuration>
                        </execution>
                    </executions>
                </plugin>
                <plugin>
                    <groupId>org.basepom.maven</groupId>
                    <artifactId>duplicate-finder-maven-plugin</artifactId>
                    <configuration>
                        <checkTestClasspath>false</checkTestClasspath>
                        <exceptions>
                            <exception>
                                <resources>
                                    <resource>draftv4/schema</resource>
                                </resources>
                                <conflictingDependencies>
                                    <dependency>
                                        <groupId>com.github.java-json-tools</groupId>
                                        <artifactId>json-schema-core</artifactId>
                                    </dependency>
                                    <dependency>
                                        <groupId>com.github.java-json-tools</groupId>
                                        <artifactId>json-schema-validator</artifactId>
                                    </dependency>
                                </conflictingDependencies>
                            </exception>
                            <exception>
                                <resources>
                                    <resource>log4j2-test.properties</resource>
                                </resources>
                            </exception>
                            <exception>
                                <resources>
                                    <resource>mozilla/public-suffix-list.txt</resource>
                                </resources>
                                <conflictingDependencies>
                                    <dependency>
                                        <groupId>org.apache.httpcomponents.client5</groupId>
                                        <artifactId>httpclient5</artifactId>
                                    </dependency>
                                    <dependency>
                                        <groupId>org.apache.httpcomponents</groupId>
                                        <artifactId>httpclient</artifactId>
                                    </dependency>
                                </conflictingDependencies>
                            </exception>
                        </exceptions>
                    </configuration>
                </plugin>
            </plugins>
        </pluginManagement>
        <plugins>
            <plugin>
                <groupId>com.diffplug.spotless</groupId>
                <artifactId>spotless-maven-plugin</artifactId>
                <configuration>
                    <java>
                        <googleJavaFormat>
                            <!-- force 1.7 while we have to support java8 builds -->
                            <version>1.7</version>
                        </googleJavaFormat>
                    </java>
                </configuration>
                <executions>
                    <execution>
                        <goals>
                            <goal>check</goal>
                        </goals>
                        <phase>verify</phase>
                    </execution>
                </executions>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-failsafe-plugin</artifactId>
            </plugin>
            <!-- TODO: drop once the parent pom is upgraded -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-resources-plugin</artifactId>
                <version>3.3.1</version>
            </plugin>
        </plugins>
    </build>
</project>

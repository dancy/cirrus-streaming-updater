cp -r common/src/test/resources/schema_repo $1/resources
cp -r cirrus-streaming-updater-producer/src/test/resources/schema_repo/* $1/resources/schema_repo
cp -r cirrus-streaming-updater-producer/src/test/resources/event-stream-config.json $1/resources
